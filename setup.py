import setuptools

setuptools.setup(
    name="cloud_portfolio_manager",
    version="1.0.1",
    description="Cloud Portfolio Manager",
    packages=["cloud_portfolio_manager"],
    package_data={"cloud_portfolio_manager": ["models/*", "optimizers/*", "portfolio/*", "util/*"]},
    install_requires=["atomicwrites==1.4.1", "attrs==23.1.0", "colorama==0.4.6", "iniconfig==2.0.0", "numpy==1.22.4", "packaging==23.2", "pandas==1.3.5",
                      "pluggy==0.13.1", "py==1.10.0", "pyparsing==2.4.7", "python-dateutil==2.8.2", "pytz==2021.1", "six==1.16.0", "scipy==1.7.2",
                      "pytest-mock==3.6.1"],
    python_requires=">=3.9",
)
