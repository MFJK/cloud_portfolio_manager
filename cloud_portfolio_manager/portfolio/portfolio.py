import logging
import uuid

import numpy as np
import scipy.stats as st
import pandas as pd

from typing import Tuple, Union
from pathlib import Path

from cloud_portfolio_manager.models.instance import Instance

pd.options.mode.chained_assignment = "raise"


class Portfolio:

    def __init__(self, time_unit: Tuple[int, str] = (1, "H"), min_quality_of_service: float = 0.95):
        self.logger = logging.getLogger(__name__)
        self.portfolio_id = str(uuid.uuid4())
        self.time_unit = time_unit
        self.min_quality_of_service = min_quality_of_service

        self.instances = pd.DataFrame(index=pd.Index([], name="instance_id"),
                                      columns=["instance_id", "beginning_time", "ending_time", "instance_type_id", "market_space", "capacity",
                                               "price", "availability_start", "availability_end", "utilization_total", "utilization_rate",
                                               "utilization_efficiency"])

        self.applications = pd.DataFrame(index=pd.Index([], name="application_id"),
                                         columns=["application_id", "starting_time", "finishing_time", "res_demand", "res_demand_deviation", "preemptible"])

        packing_index = pd.MultiIndex(levels=[[], [], []], codes=[[], [], []], names=["time_step", "instance_id", "application_id"])
        self.packing_pattern = pd.DataFrame(index=packing_index, columns=["assigned"])

        usage_index = pd.MultiIndex(levels=[[], []], codes=[[], []], names=["time_step", "instance_id"])
        self.instance_usage = pd.DataFrame(index=usage_index, columns=["agg_demand", "agg_variance", "capacity"])

    def check_application_assignment_validity(self):

        applications = {}
        for application_id, application in self.applications.iterrows():

            applications[application_id] = {}

            # Check how many time slots calculate the number of time slots it should be assigned
            applications[application_id]["intended_assignments"] = 1 + (application["finishing_time"] - application["starting_time"]) / pd.Timedelta(
                *self.time_unit)

            assignments = self.packing_pattern[self.packing_pattern.index.get_level_values(2) == application_id]

            # Check if the packing pattern contains any duplicate entries for specific time slots
            if assignments.index.get_level_values(0).has_duplicates:
                applications[application_id]["duplicates"] = assignments[assignments.index.get_level_values(0).duplicated()].index
            else:
                applications[application_id]["duplicates"] = None

            # Check if the packing pattern contains the correct number of assignments
            applications[application_id]["actual_assignments"] = len(assignments)

            intended_time_slots = pd.date_range(application["starting_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}"),
                                                application["finishing_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}"),
                                                freq=f"{self.time_unit[0]}{self.time_unit[1]}")

            if not all(assignments.index.get_level_values(0).sort_values() == intended_time_slots) \
                    or (not application["preemptible"] and len(assignments.index.unique(level=1)) != 1):
                applications[application_id]["invalid_assignments"] = True
            else:
                applications[application_id]["invalid_assignments"] = False

        instances = {}
        for instance_id, instance in self.instances.iterrows():
            instances[instance_id] = {}

            assignments = self.packing_pattern[self.packing_pattern.index.get_level_values(1) == instance_id]
            if len(assignments) == 0 or max(assignments.index.get_level_values(0)) > instance["ending_time"]:
                instances[instance_id]["invalid_assignments"] = True
            else:
                instances[instance_id]["invalid_assignments"] = False

            usage = self.instance_usage[self.instance_usage.index.get_level_values(1) == instance_id]
            usage = usage.round({"agg_demand": 4, "agg_variance": 4})
            if len(usage) == 0 or (all(usage["agg_demand"] == 0) and all(usage["agg_variance"] == 0)):
                instances[instance_id]["invalid_usage"] = True
            else:
                instances[instance_id]["invalid_usage"] = False

        status = {
            "instances": instances,
            "applications": applications,
            "number_of_applications": len(self.applications),
            "within_qos": self.__get_fitting_mask(self.instance_usage["agg_demand"], self.instance_usage["agg_variance"], self.instance_usage["capacity"]).all()
        }

        return status

    def __get_fitting_mask(self, agg_demand, agg_variance, capacity):
        probs = st.norm.cdf(((capacity - agg_demand) / np.sqrt(agg_variance.replace(0, 1).astype(float))).astype(float))

        return (probs >= self.min_quality_of_service) | ((capacity - agg_demand > 0) & (agg_variance == 0))

    def __get_allocation_groups(self, instance_id, application_id, from_time=None, to_time=None):

        conditions = []
        if from_time is not None:
            conditions.append(self.packing_pattern.index.get_level_values(0) >= from_time)

        if to_time is not None:
            conditions.append(self.packing_pattern.index.get_level_values(0) <= to_time)

        conditions.append(self.packing_pattern.index.get_level_values(1) == instance_id)
        conditions.append(self.packing_pattern.index.get_level_values(2) == application_id)

        # Get all periods where the app is assigned to the specific instance
        allocation_periods = self.packing_pattern[np.logical_and.reduce(conditions)]

        # Mark all timestamps, which are at the beginning of a consecutive series
        alloc_starts = allocation_periods.index.get_level_values(level=0).to_series().diff(1) != pd.Timedelta(*self.time_unit)

        # Mark all timestamps, which are at the end of a consecutive series
        alloc_ends = alloc_starts.cumsum().diff(-1) != 0

        # Merge both series to find the periods, where the app has been assigned to the instance (may not be sorted)
        return list(zip(alloc_starts[alloc_starts[:] == True].index.get_level_values(0), alloc_ends[alloc_ends[:] == True].index.get_level_values(0)))

    def __update_instance_utilization(self, counts, demand, add=True):

        def __calc_utilization(row, demand, add, counts, time_unit):

            # Update the total utilization average (disregarding the std. deviation) of the instance
            if add:
                row["utilization_total"] += demand * counts[row["instance_id"]]
            else:
                row["utilization_total"] -= demand * counts[row["instance_id"]]

            # Update utilization rate of instances
            row["utilization_rate"] = row["utilization_total"] / (
                    row["capacity"] * (1 + ((row["ending_time"] - row["beginning_time"]) / pd.Timedelta(*time_unit))))

            if row["market_space"] == "reserved":
                price_per_time = row["price"] / (1 + ((row["ending_time"] - row["beginning_time"]) / pd.Timedelta(*time_unit)))
                row["utilization_efficiency"] = row["utilization_rate"] / price_per_time
            else:
                row["utilization_efficiency"] = row["utilization_rate"] / row["price"]

            return row

        self.instances.loc[counts.keys()] = self.instances.loc[counts.keys()].apply(axis=1,
                                                                                    func=lambda row: __calc_utilization(row,
                                                                                                                        demand,
                                                                                                                        add,
                                                                                                                        counts,
                                                                                                                        self.time_unit))

    def __update_time_slot_utilization(self, updates=None, instance_id=None, from_time=None, to_time=None, demand=None, deviation=None, add=True):

        if None not in (instance_id, from_time, to_time, demand, deviation):
            mask = ((self.instance_usage.index.get_level_values(0) >= from_time) &
                    (self.instance_usage.index.get_level_values(0) <= to_time) &
                    (self.instance_usage.index.get_level_values(1) == instance_id))
            if add:
                self.instance_usage.loc[mask, ["agg_demand", "agg_variance"]] += demand, deviation ** 2
            else:
                self.instance_usage.loc[mask, ["agg_demand", "agg_variance"]] -= demand, deviation ** 2

        elif updates is not None:
            if add:
                self.instance_usage.loc[updates.index, ["agg_demand", "agg_variance"]] += updates
            else:
                self.instance_usage.loc[updates.index, ["agg_demand", "agg_variance"]] -= updates

        else:
            raise ValueError("Either specify an instance & time range or a date frame including all relevant indices.")

    def calculate_cost(self):
        def calculate_cost_per_time(starting_time, ending_time, cost_per_time_step):
            allocation_time = ending_time - starting_time
            # + 1 because the ending time should also be included (is included in date_range)
            number_of_time_steps = 1 + (allocation_time / pd.Timedelta(value=self.time_unit[0], unit=self.time_unit[1]))
            return cost_per_time_step * number_of_time_steps

        # Reserved instances have a total cost associated with them
        reserved_cost = self.instances.loc[self.instances["market_space"] == "reserved", "price"].sum()
        rates = self.instances.loc[self.instances["market_space"] != "reserved"].apply(
            lambda row: calculate_cost_per_time(row["beginning_time"], row["ending_time"], row["price"]), axis=1)

        if not rates.empty:
            total_cost = reserved_cost + rates.sum()
        else:
            total_cost = reserved_cost

        return total_cost

    def get_stats(self):

        time_steps = self.instances.apply(lambda row: 1 + ((row["ending_time"] - row["beginning_time"]) / pd.Timedelta(*self.time_unit)), axis=1)
        weighted_util_rate = self.instances["utilization_rate"] * time_steps

        stats = {
            "number_of_hosts_overall": len(self.instances),
            "number_of_hosts_reserved": len(self.instances[self.instances["market_space"] == "reserved"]),
            "number_of_hosts_on_demand": len(self.instances[self.instances["market_space"] == "on_demand"]),
            "number_of_hosts_spot": len(self.instances[self.instances["market_space"] == "spot"]),
            "mean_utilization_overall": np.round(self.instances["utilization_rate"].mean(), 4),
            "mean_utilization_reserved": np.round(self.instances[self.instances["market_space"] == "reserved"]["utilization_rate"].mean(), 4),
            "mean_utilization_on_demand": np.round(self.instances[self.instances["market_space"] == "on_demand"]["utilization_rate"].mean(), 4),
            "mean_utilization_spot": np.round(self.instances[self.instances["market_space"] == "spot"]["utilization_rate"].mean(), 4),
            "weighted_utilization_overall": np.round(weighted_util_rate.sum() / time_steps.sum(), 4),
            "weighted_utilization_reserved": np.round(weighted_util_rate[(self.instances["market_space"] == "reserved")].sum() /
                                                      time_steps[self.instances["market_space"] == "reserved"].sum(), 4),
            "weighted_utilization_on_demand": np.round(weighted_util_rate[self.instances["market_space"] == "on_demand"].sum() /
                                                       time_steps[self.instances["market_space"] == "on_demand"].sum(), 4),
            "weighted_utilization_spot": np.round(weighted_util_rate[self.instances["market_space"] == "spot"].sum() /
                                                  time_steps[self.instances["market_space"] == "spot"].sum(), 4),
            "cost_overall": self.calculate_cost()
        }

        return stats

    def save(self, path: Union[str, Path]):

        if isinstance(path, str):
            path = Path(path)

        self.instance_usage.to_csv(path.joinpath("instance_usage.csv"))
        self.applications.to_csv(path.joinpath("applications.csv"))
        self.instances.to_csv(path.joinpath("instances.csv"))
        self.packing_pattern.to_csv(path.joinpath("packing_pattern.csv"))

    def fits(self, application, from_time, to_time, instance, without=None):

        # The app does not fit the available instance times. No need for further checks
        if application.starting_time < instance.beginning_time or application.finishing_time > instance.ending_time:
            return False

        # Make a copy of the instance usage, as some temporary data modifications are necessary.
        tmp_instance_usage = self.instance_usage.copy()

        ignore = {application.application_id}
        ignore.update(without or [])

        for application_id in ignore:
            try:
                ignore_app = self.applications.loc[application_id]
            except KeyError:
                # Application has not yet been assigned
                continue

            # Find consecutive periods, where the app is assigned to the specific instance
            groups = self.__get_allocation_groups(instance_id=instance.instance_id, application_id=application_id)

            # Remove the application usage from the instance for the specified periods
            for start, end in groups:
                tmp_instance_usage.loc[(tmp_instance_usage.index.get_level_values(0) >= start) &
                                       (tmp_instance_usage.index.get_level_values(0) <= end) &
                                       (tmp_instance_usage.index.get_level_values(1) == instance.instance_id),
                                       ["agg_demand", "agg_variance"]] -= ignore_app.res_demand, ignore_app.res_demand_deviation ** 2

        # Define the scope for which to perform the evaluation
        mask = ((tmp_instance_usage.index.get_level_values(0) >= from_time) &
                (tmp_instance_usage.index.get_level_values(0) <= to_time) &
                (tmp_instance_usage.index.get_level_values(1) == instance.instance_id))

        # Add the additional resource demands to the relevant scope
        tmp_instance_usage.loc[mask, ["agg_demand", "agg_variance"]] += application.res_demand, application.res_demand_deviation ** 2

        within_limits = self.__get_fitting_mask(agg_demand=tmp_instance_usage.loc[mask, "agg_demand"],
                                                agg_variance=tmp_instance_usage.loc[mask, "agg_variance"],
                                                capacity=tmp_instance_usage.loc[mask, "capacity"])

        if len(within_limits) == 0 or not within_limits.all():  # noqa
            return False
        else:
            return True

    def dominates(self, instance_id, dominating_id, dominated_ids, from_time, to_time):

        if not dominated_ids:
            raise ValueError("No IDs for dominated applications provided.")

        dominating_app = self.applications.loc[dominating_id]

        # The starting time of the dominating app has to be earlier (or equal) than the earliest assignment to the instance
        earliest_assignment = min(self.packing_pattern.loc[slice(None), instance_id, dominated_ids].index.unique(level=0))
        if from_time > earliest_assignment:
            return False

        # The finishing time fo the dominating app has to be later (or equal) than the latest assignment to the instance
        latest_assignment = max(self.packing_pattern.loc[slice(None), instance_id, dominated_ids].index.unique(level=0))
        if to_time < latest_assignment:
            return False

        # The probability that the dominating app has a higher demand than the dominated ones needs to be greater than 50%
        # Based on: https://stats.stackexchange.com/questions/50501/probability-of-one-random-variable-being-greater-than-another/431484
        dom_demand = self.applications.loc[dominated_ids]["res_demand"].sum()
        dom_variance = (self.applications.loc[dominated_ids]["res_demand_deviation"] ** 2).sum()
        # Calculate the variance of the new distribution (assuming a normal distribution)
        new_variance = round((dom_variance + (dominating_app["res_demand_deviation"] ** 2)).astype(float), 4)

        dom_probability = 1 - st.norm.cdf(((-(dominating_app["res_demand"] - dom_demand)) / np.sqrt(new_variance or 1).astype(float)).astype(float))
        if (new_variance == 0 and dom_demand > dominating_app["res_demand"]) or (new_variance != 0 and dom_probability < 0.5):
            return False

        return self.fits(dominating_app, from_time, to_time, self.instances.loc[instance_id], without=dominated_ids)

    def get_applications(self, time_steps=None, instance_ids=None, application_ids=None):

        if time_steps is not None and instance_ids is not None and application_ids is not None:
            raise ValueError("Cannot set time_steps/instance_ids and application_ids together.")

        cond = None
        if time_steps is not None or instance_ids is not None:
            try:
                instance_ids = instance_ids if len(instance_ids) > 0 else slice(None)
                cond = self.packing_pattern.loc[(time_steps or slice(None), instance_ids, slice(None)), :].index.unique(level=2)
            except KeyError:
                self.logger.debug(f"No assignments for {time_steps} - {instance_ids}")
        elif application_ids is not None:
            cond = application_ids

        if cond is not None:
            return self.applications[self.applications.index.isin(cond)]
        else:
            return self.applications

    def add_application(self, instance_id, application, from_time=None, to_time=None, append=True):
        self.logger.debug(f"Assigning app {application.application_id} to instance {instance_id} ({application.starting_time} - {application.finishing_time})")

        if append:
            self.applications.loc[application.application_id] = application.to_dict()

        start = from_time if from_time is not None else application.starting_time
        start = start.floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")
        end = to_time if to_time is not None else application.finishing_time
        end = end.floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")

        date_range = pd.date_range(start=start, end=end, freq=f"{self.time_unit[0]}{self.time_unit[1]}")
        instance_ids = [instance_id] * len(date_range)
        new_index = pd.MultiIndex.from_arrays(arrays=[date_range, instance_ids, [application.application_id] * len(date_range)],
                                              names=["time_step", "instance_id", "application_id"])
        new_assignments = pd.DataFrame(index=new_index, data=[True] * len(date_range), columns=["assigned"])

        self.packing_pattern = pd.concat([self.packing_pattern, new_assignments])

        self.__update_instance_utilization(counts={instance_id: len(date_range)}, demand=application.res_demand)
        self.__update_time_slot_utilization(instance_id=instance_id, from_time=start, to_time=end,
                                            demand=application.res_demand, deviation=application.res_demand_deviation)

    def bulk_add_application(self, application, time_steps, instance_ids, append=True):

        if len(time_steps) != len(instance_ids):
            raise ValueError("time_steps and instance_ids must be same length")

        # Add application to store
        if append:
            self.applications.loc[application.application_id] = application.to_dict()

        # Create a new assignment df with the time steps, instance ids and app id as index
        new_index = pd.MultiIndex.from_arrays(arrays=[time_steps, instance_ids, [application.application_id] * len(time_steps)],
                                              names=["time_step", "instance_id", "application_id"])
        new_assignments = pd.DataFrame(index=new_index, data=[True] * len(time_steps), columns=["assigned"])

        # Concatenate the new assignments to the packing pattern
        self.packing_pattern = pd.concat([self.packing_pattern, new_assignments])

        self.__update_instance_utilization(counts=new_assignments.index.get_level_values(1).value_counts().to_dict(), demand=application.res_demand)

        res_demand_variance = application.res_demand_deviation ** 2
        update_index = pd.MultiIndex.from_arrays(arrays=[time_steps, instance_ids], names=["res_demand", "res_demand_variance"])
        updates = pd.DataFrame(index=update_index, data={"agg_demand": [application.res_demand] * len(time_steps),
                                                         "agg_variance": [res_demand_variance] * len(time_steps)})

        self.__update_time_slot_utilization(updates)

    def delete_applications(self, application_ids, from_store=True, from_packing_pattern=True, from_time=None, to_time=None):

        if from_packing_pattern:

            indices_to_drop = pd.MultiIndex(levels=[[], [], []], codes=[[], [], []], names=["time_step", "instance_id", "application_id"])

            from_cond = None
            if from_time is not None:
                from_cond = self.packing_pattern.index.get_level_values(0) >= from_time

            to_cond = None
            if to_time is not None:
                to_cond = self.packing_pattern.index.get_level_values(0) <= to_time

            for application_id in application_ids:
                application = self.applications.loc[application_id]
                assigned_instances = self.packing_pattern.loc[(slice(None), slice(None), application_id)].index.unique(level=1)
                for instance_id in assigned_instances:
                    # Check how many time slots the application has been assigned to the specific instance
                    conditions = []

                    if from_cond is not None:
                        conditions.append(from_cond)

                    if to_cond is not None:
                        conditions.append(to_cond)

                    conditions.append(self.packing_pattern.index.isin([instance_id], level=1))
                    conditions.append(self.packing_pattern.index.isin([application_id], level=2))

                    number_of_time_slots = len(self.packing_pattern[np.logical_and.reduce(conditions)])

                    self.__update_instance_utilization(counts={instance_id: number_of_time_slots}, demand=application.res_demand, add=False)

                    # Update the instance usage
                    groups = self.__get_allocation_groups(instance_id, application_id, from_time=from_time, to_time=to_time)
                    for start, end in groups:
                        self.__update_time_slot_utilization(instance_id=instance_id,
                                                            from_time=start,
                                                            to_time=end,
                                                            demand=application.res_demand,
                                                            deviation=application.res_demand_deviation,
                                                            add=False)

                    time_steps = pd.date_range(from_time or application.starting_time,
                                               to_time or application.finishing_time,
                                               freq=f"{self.time_unit[0]}{self.time_unit[1]}")
                    instance_indices = pd.MultiIndex.from_arrays(arrays=[time_steps, [instance_id] * len(time_steps), [application_id] * len(time_steps)],
                                                                 names=["time_steps", "instance_id", "application_id"])

                    indices_to_drop = indices_to_drop.union(instance_indices)

            self.packing_pattern.drop(index=indices_to_drop, inplace=True, errors="ignore")

        if from_store:
            self.applications = self.applications.drop(application_ids)

    def get_instances(self, instance_ids=None, from_time=None, to_time=None, full_cover=True, demand=None, deviation=None):

        # Instances have to cover the entire time span
        if from_time and to_time and full_cover:
            instances = self.instances.loc[(self.instances["beginning_time"] <= from_time) & (self.instances["ending_time"] >= to_time)]
        # Instances do not have to cover the entire time span. Partial coverage is enough.
        elif from_time and to_time and not full_cover:
            instances = self.instances.loc[((self.instances["beginning_time"] <= from_time) & (self.instances["ending_time"] >= to_time)) |
                                           ((self.instances["beginning_time"] >= from_time) & (self.instances["beginning_time"] <= to_time)) |
                                           ((self.instances["ending_time"] >= from_time) & (self.instances["ending_time"] <= to_time))]
        elif instance_ids is not None:
            instances = self.instances[self.instances.index.isin(instance_ids)]
        else:
            instances = self.instances

        if (demand is not None and deviation is None) or (demand is None and deviation is not None):
            raise ValueError("Demand and deviation both have to be set.")

        if None not in (demand, deviation):

            if full_cover:
                # Calculate the number of required timeslots
                required_time_slots = 0
                if from_time and to_time:
                    required_time_slots = (to_time - from_time) / pd.Timedelta(value=self.time_unit[0], unit=self.time_unit[1])

                # Calculate the number of time slots per instance, where the requirement is fulfilled
                candidates = self.instance_usage.loc[(from_time <= self.instance_usage.index.get_level_values(0)) &
                                                     (self.instance_usage.index.get_level_values(0) <= to_time)].copy()
                candidates[["agg_demand", "agg_variance"]] += demand, deviation ** 2  # noqa
                fitting_mask = self.__get_fitting_mask(candidates["agg_demand"], candidates["agg_variance"], candidates["capacity"])
                candidates = candidates[fitting_mask]
                candidate_time_slots = candidates.index.get_level_values(1).value_counts()
                candidate_ids = candidate_time_slots[candidate_time_slots > required_time_slots].index.unique(0)

            else:
                candidates = self.instance_usage.loc[(from_time <= self.instance_usage.index.get_level_values(0)) &
                                                     (self.instance_usage.index.get_level_values(0) <= to_time)].copy()
                candidates[["agg_demand", "agg_variance"]] += demand, deviation ** 2  # noqa
                fitting_mask = self.__get_fitting_mask(candidates["agg_demand"], candidates["agg_variance"], candidates["capacity"])
                candidates = candidates[fitting_mask]
                candidate_ids = candidates.index.unique(1)

            instances = instances.loc[instances.index.isin(candidate_ids), :]

        return instances

    def get_instance_usage_over_time(self, from_time, to_time, demand, deviation, drop_duplicates=True, sorted=True):
        # Filter for those instances that fall within the desired time frame
        instances = self.instance_usage.loc[(self.instance_usage.index.get_level_values(0) >= from_time) &
                                            (self.instance_usage.index.get_level_values(0) <= to_time)].copy()

        # Filter for those instances where the probability that the total usage stays within the desired limit
        if not instances.empty:
            tmp_instances = instances.copy()
            tmp_instances[["agg_demand", "agg_variance"]] += demand, deviation ** 2
            fitting_mask = self.__get_fitting_mask(tmp_instances["agg_demand"], tmp_instances["agg_variance"], tmp_instances["capacity"])
            instances = instances.loc[fitting_mask]

        if drop_duplicates:
            instances = instances[~instances.index.get_level_values(0).duplicated()]

        if sorted:
            instances = instances.sort_values("time_step", ascending=True)

        return instances

    def add_instance(self, instance):
        self.logger.debug(f"Adding instance {instance.instance_id}.")

        # Ensure dynamic parameters are reset when inheriting from another individual (e.g. crossover)
        instance.utilization_total = 0
        instance.utilization_rate = 0
        instance.utilization_efficiency = 0

        # Add instance to data store
        self.instances.loc[instance.instance_id] = instance.to_dict(flat=True) if isinstance(instance, Instance) else instance

        # Add instance to usage store. Make sure only the instance is not added to the store for timeslots it does not fully cover.
        capacity = instance.instance_type.capacity if isinstance(instance, Instance) else instance.capacity
        date_range = pd.date_range(start=instance.beginning_time.ceil(freq=f"{self.time_unit[0]}{self.time_unit[1]}"),
                                   end=instance.ending_time.floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}"),
                                   freq=f"{self.time_unit[0]}{self.time_unit[1]}")
        new_index = pd.MultiIndex.from_arrays(arrays=[date_range, [instance.instance_id] * len(date_range)], names=["time_step", "instance_id"])
        new_assignments = pd.DataFrame(index=new_index, data={"agg_demand": [0] * len(date_range),
                                                              "agg_variance": [0] * len(date_range),
                                                              "capacity": [capacity] * len(date_range)})

        self.instance_usage = pd.concat([self.instance_usage, new_assignments])

    def delete_instances(self, instance_ids, from_store=True, from_packing_pattern=True):
        if from_store:
            self.instances.drop(instance_ids, inplace=True, errors="ignore")

        if from_packing_pattern:
            self.packing_pattern.drop(instance_ids, level=1, inplace=True, errors="ignore")
            self.instance_usage.drop(instance_ids, level=1, inplace=True, errors="ignore")

    def get_packing_pattern(self, instance_ids=None, application_ids=None, from_time=None, to_time=None):
        conditions = []

        if instance_ids is not None:
            conditions.append(self.packing_pattern.index.isin(instance_ids, level=1))

        if application_ids is not None:
            conditions.append(self.packing_pattern.index.isin(application_ids, level=2))

        if from_time is not None:
            conditions.append(self.packing_pattern.index.get_level_values(0) >= from_time)

        if to_time is not None:
            conditions.append(self.packing_pattern.index.get_level_values(0) <= to_time)

        if conditions is not None:
            return self.packing_pattern[np.logical_and.reduce(conditions)].copy()
        else:
            return self.packing_pattern.copy()

    def get_assignment_gaps(self, application):

        # Get the packing pattern for the respective application
        assignments = self.get_packing_pattern(application_ids=[application.application_id])

        time_slots = pd.date_range(start=application.starting_time, end=application.finishing_time, freq=f"{self.time_unit[0]}{self.time_unit[1]}")

        unassigned_time_slots = time_slots.difference(assignments.index.get_level_values(0))
        gaps = (unassigned_time_slots.to_series().diff(1) != pd.Timedelta(value=self.time_unit[0], unit=self.time_unit[1])).cumsum()

        return gaps.groupby(gaps)
