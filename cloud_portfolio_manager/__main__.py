import argparse
import itertools
import threading
import time
import sys

from cloud_portfolio_manager import Georg, Erich

done = False


# Based on: https://www.codegrepper.com/code-examples/python/python+show+loading+symbol
def processing():
    for c in itertools.cycle(["|", "/", "-", "\\"]):
        if done:
            break
        sys.stdout.write(f"\r{c}")
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write("\rFinished processing!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Cloud Portfolio Manager")

    # Positional arguments (required)
    parser.add_argument("optimizer", type=str, choices={"erich", "georg"}, help="The optimizer approach to use.")
    parser.add_argument("applications", type=str, help="The path to the application file to use.")
    parser.add_argument("instance_types", type=str, help="The path to the instance type file to use.")
    parser.add_argument("packing_pattern_output", type=str, help="The output path for the packing pattern.")
    parser.add_argument("instances_output", type=str, help="The output path for the instances.")

    # Named arguments (optional)
    parser.add_argument("-p", "--population_size", default=20, type=int, help="GEORG: The number of individuals in a population. Default: 20")

    parser.add_argument("-g", "--number_of_generations", default=15, type=int, help="GEORG: The max. number of generations to compute. Default: 15")

    parser.add_argument("-i", "--max_generations_without_improvements", default=2, type=int,
                        help="GEORG: The number of consecutive generations without improvements after which to abort. Default: 2")

    parser.add_argument("-c", "--convergence_threshold", default=0, type=float,
                        help="GEORG: Abort evolution process once the difference between the best individual's fitness and the average population "
                             "fitness falls below the convergence threshold. Default: 0")

    parser.add_argument("-s", "--parent_selection_strategy", default="roulette_wheel", type=str, choices={"roulette_wheel", "tournament", "random"},
                        help="GEORG: The strategy to apply when choosing parents for the crossover operator. Default: roulette_wheel")

    parser.add_argument("-o", "--number_of_offspring", default=10, type=int, help="GEORG: The number of offspring to create in each generation. Default: 10")

    parser.add_argument("-m", "--mutation_rate", default=0.05, type=float, help="GEORG: The likeliness of mutating the chromosome of new offspring. "
                                                                                "Default: 0.05")

    parser.add_argument("-n", "--number_of_mutations", default=1, type=int, help="GEORG: The number of mutations perform for a selected individual. Default: 1")

    parser.add_argument("-e", "--survivor_selection_strategy", default="fittest_overall", type=str, choices={"fittest_overall", "fittest_current"},
                        help="GEORG: The strategy to apply when selecting individuals to pass on to the next generation. Default: fittest_overall")

    parser.add_argument("-t", "--time_unit", default="1H", type=str, help="The time resolution to be used. Default: 1H")

    parser.add_argument("-q", "--min_quality_of_service", default=0.95, type=float,
                        help="The desired probability with which the aggregated resource demand of an instance stays within the capacity limits.")

    args = parser.parse_args()

    if args.instances_output == args.packing_pattern_output:
        raise ValueError("Outputs for instances and packing pattern must not be the same.")

    if not all([path[-3:].lower() == "csv" for path in [args.applications, args.instance_types, args.instances_output, args.packing_pattern_output]]):
        raise ValueError("Input/output file formats must be 'csv'.")

    if not len(args.time_unit) == 2:
        raise ValueError("Time unit should be a string of length 2. Example: 1H, 1D")

    if not 0 <= args.min_quality_of_service <= 1:
        raise ValueError("The desired quality of service needs to be specified as a probability value in the range of [0, ..., 1]")

    time_unit = (int(args.time_unit[0]), args.time_unit[1])
    options = {}
    optimizer = None

    if args.optimizer == "erich":
        optimizer = Erich(time_unit=time_unit, min_quality_of_service=args.min_quality_of_service)
    elif args.optimizer == "georg":
        optimizer = Georg(time_unit=time_unit, min_quality_of_service=args.min_quality_of_service)
        params = ["population_size", "number_of_generations", "max_generations_without_improvements", "convergence_threshold", "parent_selection_strategy",
                  "number_of_offspring", "mutation_rate", "number_of_mutations", "survivor_selection_strategy"]

        for param in params:
            options[param] = args.__getattribute__(param)

    optimizer.load_applications(path=args.applications)
    optimizer.load_instance_types(path=args.instance_types)

    thread = threading.Thread(target=processing)
    thread.start()

    optimization = optimizer.optimize(**options)

    done = True

    if args.optimizer == "georg":
        # Disregard generational data and extract only the best solution
        optimization = optimization[0][0]

    optimization.instances.drop("instance_id", axis=1, inplace=True)
    optimization.instances.to_csv(args.instances_output)
    optimization.packing_pattern.drop("assigned", axis=1, inplace=True)
    optimization.packing_pattern.to_csv(args.packing_pattern_output)
