from pandas import Timestamp

from cloud_portfolio_manager.models.instance_type import InstanceType


class Instance:

    def __init__(self,
                 instance_id: str,
                 instance_type: InstanceType,
                 beginning_time: Timestamp = None,
                 ending_time: Timestamp = None):

        self.instance_id: str = instance_id
        self.instance_type: InstanceType = instance_type
        self.utilization_total = 0
        self.utilization_rate = 0
        self.utilization_efficiency = 0

        if self.instance_type.market_space == "reserved":
            self.beginning_time: Timestamp = self.instance_type.availability_start
            self.ending_time: Timestamp = self.instance_type.availability_end
        elif beginning_time is not None and ending_time is not None:
            self.beginning_time: Timestamp = beginning_time
            self.ending_time: Timestamp = ending_time
        else:
            raise ValueError("Missing starting/ending time.")

    def to_dict(self, flat=False):
        result = {
            "instance_id": self.instance_id,
            "beginning_time": self.beginning_time,
            "ending_time": self.ending_time,
            "utilization_total": self.utilization_total,
            "utilization_rate": self.utilization_rate,
            "utilization_efficiency": self.utilization_efficiency
        }

        if flat:
            for key in self.instance_type.to_dict():
                result[key] = self.instance_type.__getattribute__(key)
        else:
            result["instance_type"] = self.instance_type.to_dict()

        return result
