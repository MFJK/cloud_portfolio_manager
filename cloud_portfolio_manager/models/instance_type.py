from typing import Union

import pandas as pd


class InstanceType:

    def __init__(self,
                 instance_type_id: str,
                 capacity: float,
                 market_space: str,
                 price: float,
                 availability_start: Union[pd.Timestamp, str, type(pd.NaT)] = pd.NaT,
                 availability_end: Union[pd.Timestamp, str, type(pd.NaT)] = pd.NaT):
        self.instance_type_id: str = instance_type_id
        self.market_space: str = market_space
        self.capacity: float = capacity
        self.price: float = price

        if type(availability_start) is type(pd.NaT) or isinstance(availability_start, pd.Timestamp):  # noqa
            self.availability_start = availability_start
        elif isinstance(availability_end, str):
            self.availability_start = pd.Timestamp(availability_start)  # noqa
        else:
            raise ValueError("availability_start is expected to be pd.Timestamp, pd.NaT or ISO 8601 string.")

        if type(availability_end) is type(pd.NaT) or isinstance(availability_end, pd.Timestamp):  # noqa
            self.availability_end = availability_end
        elif isinstance(availability_end, str):
            self.availability_end = pd.Timestamp(availability_end)  # noqa
        else:
            raise ValueError("availability_end is expected to be pd.Timestamp, pd.NaT or ISO 8601 string.")

    def to_dict(self):
        return {
            "instance_type_id": self.instance_type_id,
            "market_space": self.market_space,
            "capacity": self.capacity,
            "price": self.price,
            "availability_start": self.availability_start,
            "availability_end": self.availability_end
        }
