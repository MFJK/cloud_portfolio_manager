from typing import Union

import pandas as pd


class Application:

    def __init__(self,
                 application_id: str,
                 starting_time: Union[pd.Timestamp, str],
                 finishing_time: Union[pd.Timestamp, str],
                 res_demand: float,
                 res_demand_deviation: float,
                 preemptible: bool):
        self.application_id: str = application_id
        self.res_demand: float = float(res_demand)
        self.res_demand_deviation: float = float(res_demand_deviation)
        self.preemptible: bool = preemptible

        if isinstance(starting_time, pd.Timestamp):
            self.starting_time = starting_time
        elif isinstance(starting_time, str):
            self.starting_time = pd.Timestamp(starting_time)  # noqa
        else:
            raise ValueError("starting_time is expected to be pd.Timestamp or ISO 8601 string.")

        if isinstance(finishing_time, pd.Timestamp):
            self.finishing_time = finishing_time
        elif isinstance(finishing_time, str):
            self.finishing_time = pd.Timestamp(finishing_time)  # noqa
        else:
            raise ValueError("finishing_time is expected to be pd.Timestamp or ISO 8601 string.")

    def to_dict(self):
        return {
            "application_id": self.application_id,
            "starting_time": self.starting_time,
            "finishing_time": self.finishing_time,
            "res_demand": self.res_demand,
            "res_demand_deviation": self.res_demand_deviation,
            "preemptible": self.preemptible
        }
