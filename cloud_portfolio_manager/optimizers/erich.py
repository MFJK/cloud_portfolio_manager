import uuid
import copy

import pandas as pd

from cloud_portfolio_manager.models import Instance, InstanceType, Application
from cloud_portfolio_manager.optimizers import PortfolioOptimizer
from cloud_portfolio_manager.portfolio import Portfolio
from cloud_portfolio_manager.util import OptimizerException


class Erich(PortfolioOptimizer):

    def optimize(self,
                 portfolio: Portfolio = None):

        # Copy data to create new DataFrames (opposed to simple views)
        non_preemptible = self.applications.loc[self.applications["preemptible"] == False].copy()
        preemptible = self.applications[self.applications["preemptible"] == True].copy()
        reserved = self.instance_types[self.instance_types["market_space"] == "reserved"].copy()
        on_demand = self.instance_types[self.instance_types["market_space"] == "on_demand"].copy()
        spot = self.instance_types[self.instance_types["market_space"] == "spot"].copy()

        # Sort applications by increasing starting time and non-increasing res. demand variance
        non_preemptible = non_preemptible.sort_values(["starting_time", "res_demand_deviation"], ascending=[True, False])
        preemptible = preemptible.sort_values(["starting_time", "res_demand_deviation"], ascending=[True, False])

        # Sort reserved instances by increasing cost per capacity and time slot
        reserved["price_capacity"] = reserved["price"] / reserved["capacity"]
        reserved["total_time"] = reserved["availability_end"] - reserved["availability_start"]
        reserved["time_slots"] = reserved["total_time"] / pd.Timedelta(value=self.time_unit[0], unit=self.time_unit[1])
        reserved["sort_key"] = reserved["price_capacity"] / reserved["time_slots"]
        reserved = reserved.sort_values("sort_key", ascending=True)

        # Sort on-demand instances by increasing cost per capacity and time slot
        # The price of on-demand instance is already per time unit
        on_demand["sort_key"] = on_demand["price"] / on_demand["capacity"]
        on_demand = on_demand.sort_values("sort_key", ascending=True)

        # Sort spot instances by increasing cost per capacity and time slot
        # The price of spot instances is already per time unit
        spot["sort_key"] = spot["price"] / spot["capacity"]
        spot = spot.sort_values("sort_key", ascending=True)

        # Define the portfolio, which will eventually hold the completed packing pattern
        portfolio = Portfolio(time_unit=self.time_unit, min_quality_of_service=self.min_quality_of_service) if portfolio is None else portfolio

        ##################
        #    Reserved    #
        ##################
        # Assign non-preemptible applications to reserved instances
        for app_id, app in non_preemptible.iterrows():
            starting_time = app["starting_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")
            finishing_time = app["finishing_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")

            # Get instances which would cover the entire time span
            candidate_instances = portfolio.get_instances(from_time=starting_time, to_time=finishing_time)

            assigned_instance = None
            for candidate_id, instance in candidate_instances.iterrows():
                # Check if the application fits into the instance for the entire time span
                if portfolio.fits(app, starting_time, finishing_time, instance):
                    assigned_instance = instance
                    break

            # Assign app to already allocated instance
            if assigned_instance is not None:
                portfolio.add_application(assigned_instance["instance_id"], Application(**app))
            else:
                # Select a new instance from the reserved instance types and add it to the portfolio
                for instance_type_id, reserved_type in reserved.iterrows():

                    if self.fits_type(app, reserved_type):
                        new_instance = Instance(instance_id=str(uuid.uuid4()),
                                                instance_type=InstanceType(instance_type_id=reserved_type["instance_type_id"],
                                                                           capacity=reserved_type["capacity"],
                                                                           market_space=reserved_type["market_space"],
                                                                           price=reserved_type["price"],
                                                                           availability_start=reserved_type["availability_start"],
                                                                           availability_end=reserved_type["availability_end"]))
                        portfolio.add_instance(new_instance)
                        portfolio.add_application(new_instance.instance_id, Application(**app))

                        # The correct reserved instance has been selected and the app has been assigned to it.
                        break

        ##################
        #    OnDemand    #
        ##################
        # For each reserved instance get all applications
        for instance_id, instance in portfolio.get_instances().iterrows():

            # Create a new portfolio with the currently iterated instance removed
            alternative_portfolio = copy.deepcopy(portfolio)
            alternative_portfolio.delete_instances(instance_ids=[instance_id])

            # Find all apps that belonged to the removed instance
            apps_to_reshuffle = portfolio.get_applications(instance_ids=[instance_id])

            # Flag to indicate whether or not the apps have actually been reshuffled in the alternative portfolio
            reshuffled = False

            # Reinsert the remove applications into the portfolio
            try:
                for application_id, app in apps_to_reshuffle.iterrows():
                    starting_time = app["starting_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")
                    finishing_time = app["finishing_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")

                    candidate_instances = alternative_portfolio.get_instances(from_time=starting_time, to_time=finishing_time)

                    assigned_instance = None
                    for candidate_id, candidate_instance in candidate_instances.iterrows():
                        # Check if the application fits into the instance for the entire time span
                        if alternative_portfolio.fits(app, starting_time, finishing_time, candidate_instance):
                            assigned_instance = candidate_instance
                            break

                    # Reassign app to already existing instance
                    if assigned_instance is not None:
                        alternative_portfolio.add_application(assigned_instance.instance_id, Application(**app))
                        reshuffled = True
                    else:
                        # Select a new instance from the on-demand instance types and add it to the portfolio
                        for instance_type_id, on_demand_type in on_demand.iterrows():

                            # No need to check the availability times, since it is an on-demand instance
                            if self.fits_type(app, on_demand_type):
                                new_instance = Instance(instance_id=str(uuid.uuid4()),
                                                        instance_type=InstanceType(instance_type_id=on_demand_type["instance_type_id"],
                                                                                   capacity=on_demand_type["capacity"],
                                                                                   market_space=on_demand_type["market_space"],
                                                                                   price=on_demand_type["price"]),
                                                        beginning_time=starting_time,
                                                        ending_time=finishing_time)

                                alternative_portfolio.add_instance(new_instance)
                                alternative_portfolio.add_application(new_instance.instance_id, Application(**app))
                                reshuffled = True

                                # The correct reserved instance has been selected and the app has been assigned to it.
                                break

                            else:
                                raise OptimizerException(f"Cannot reshuffle. No instance with required capacity available!")

                if reshuffled and alternative_portfolio.calculate_cost() < portfolio.calculate_cost():
                    portfolio = alternative_portfolio

            # No instance with required capacity was available. Therefore, no reshuffling could take place, which means there is no need to compare portfolios.
            except OptimizerException:
                pass

        ##################
        #      Spot      #
        ##################
        # Assign preemptible applications to the portfolio. Select spot instances if no fitting instance exists
        for application_id, app in preemptible.iterrows():
            starting_time = app["starting_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")
            # Finishing time should be floored to the beginning of the last time slot
            finishing_time = app["finishing_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")

            # Get candidate instances (all possible ones that cover the time frame)
            candidate_instances = portfolio.get_instance_usage_over_time(from_time=starting_time,
                                                                         to_time=finishing_time,
                                                                         demand=app["res_demand"],
                                                                         deviation=app["res_demand_deviation"])

            # Assign app to portfolio without allocation new instances
            # Add the application to all the instances that cover the desired timeslots and still have enough capacity
            if not candidate_instances.empty:
                portfolio.bulk_add_application(application=app,
                                               time_steps=candidate_instances.index.get_level_values(level=0),
                                               instance_ids=candidate_instances.index.get_level_values(level=1))

            s = pd.Timestamp.now()
            # Find the gaps in the assigned time series of the candidate instances
            start_slots = []
            end_slots = []
            starts_filtered = None
            ends_filtered = None
            if candidate_instances.empty:
                self.logger.info("No candidates")
                start_slots.append(starting_time)
                end_slots.append(finishing_time)
            else:
                starts = candidate_instances.index.get_level_values(level=0).to_series().diff(1) != pd.Timedelta(*self.time_unit)
                ends = starts.cumsum().diff(-1) != 0
                starts_filtered = starts[starts[:] == True]
                ends_filtered = ends[ends[:] == True]

                if starting_time < starts_filtered.index[0] and finishing_time > ends_filtered.index[-1]:
                    end_slots.append(starting_time)
                    end_slots.extend(ends_filtered.index)

                    start_slots.extend(starts_filtered.index)
                    start_slots.append(finishing_time)

                elif starting_time < starts_filtered.index[0]:
                    end_slots.append(starting_time)
                    end_slots.extend(ends_filtered.index[:-1])

                    start_slots.extend(starts_filtered.index)

                elif finishing_time > ends_filtered.index[-1]:
                    start_slots.extend(starts_filtered.index[1:])
                    start_slots.append(finishing_time)

                    end_slots.extend(ends_filtered.index)
                else:
                    start_slots.extend(starts_filtered.index[1:])
                    end_slots.extend(ends_filtered.index[:-1])

            gaps = zip(start_slots, end_slots)

            self.logger.info(f"Finding gaps took: {pd.Timestamp.now() - s}")

            # Assign app to spot instances to fill all the gaps
            for gap in gaps:
                self.logger.info(f"Checking gap: {gap}")

                # Select a new instance from the on-demand instance types and add it to the portfolio
                for instance_type_id, spot_type in spot.iterrows():

                    # No need to check the availability times, since it is a spot instance
                    if self.fits_type(app, spot_type):
                        beginning_time = min(gap)
                        ending_time = max(gap)

                        # We have identified the edge assignment slots. Now we want the actual times from when to when no assignment exists.
                        if beginning_time != starting_time or (ends_filtered is not None and ends_filtered.index[0] == starting_time):
                            beginning_time += pd.Timedelta(*self.time_unit)

                        if ending_time != finishing_time or (starts_filtered is not None and starts_filtered.index[-1] == finishing_time):
                            ending_time -= pd.Timedelta(*self.time_unit)

                        new_instance = Instance(instance_id=str(uuid.uuid4()),
                                                instance_type=InstanceType(instance_type_id=spot_type["instance_type_id"],
                                                                           capacity=spot_type["capacity"],
                                                                           market_space=spot_type["market_space"],
                                                                           price=spot_type["price"]),
                                                beginning_time=beginning_time,
                                                ending_time=ending_time)

                        self.logger.info("Adding spot instance")
                        portfolio.add_instance(new_instance)
                        self.logger.info("Adding spot app")
                        portfolio.add_application(new_instance.instance_id, Application(**app), from_time=beginning_time, to_time=ending_time)

                        # The correct reserved instance has been selected and the app has been assigned to it.
                        break

        return portfolio
