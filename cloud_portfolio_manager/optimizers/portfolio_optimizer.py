import logging

import pandas as pd
import scipy.stats as st

from typing import List, Tuple, Union

from cloud_portfolio_manager.models.application import Application
from cloud_portfolio_manager.models.instance_type import InstanceType


class PortfolioOptimizer:

    def __init__(self,
                 applications: List[Application] = None,
                 instance_types: List[InstanceType] = None,
                 time_unit: Tuple[int, str] = (1, "H"),
                 min_quality_of_service: float = 0.95):

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(level=logging.DEBUG)
        self.time_unit = time_unit
        self.min_quality_of_service = min_quality_of_service

        self.applications = pd.DataFrame(data=[a.to_dict() for a in applications] if applications else [],
                                         columns=["application_id", "starting_time", "finishing_time", "res_demand", "res_demand_deviation", "preemptible"])

        if not self.applications.empty:
            self.applications = self.applications.set_index("application_id", drop=False)

        self.instance_types = pd.DataFrame(data=[i.to_dict() for i in instance_types] if instance_types else [],
                                           columns=["instance_type_id", "market_space", "capacity", "price", "availability_start", "availability_end"])

        if not self.instance_types.empty:
            self.instance_types = self.instance_types.set_index("instance_type_id", drop=False)

    def load_applications(self, path=None, applications: List[Application] = None, options=None, default_tz="UTC"):

        # Load CSV data
        if path and path.split(".")[-1].lower() == "csv":
            opts = options or {}
            self.applications = pd.read_csv(filepath_or_buffer=path, parse_dates=["starting_time", "finishing_time"], **opts)

        elif applications:
            self.applications = pd.DataFrame(data=[i.to_dict() for i in applications] if applications else [],
                                             columns=["application_id", "starting_time", "finishing_time", "res_demand", "res_demand_deviation", "preemptible"])
        else:
            raise ValueError("Can only process CSV data.")

        self.applications = self.applications.astype(dtype={"starting_time": pd.DatetimeTZDtype(tz=default_tz),
                                                            "finishing_time": pd.DatetimeTZDtype(tz=default_tz)})

        if not self.applications.empty:
            self.applications = self.applications.set_index("application_id", drop=False)

    def load_instance_types(self, path: str = None, instance_types: List[InstanceType] = None, options=None, default_tz="UTC"):

        # Load CSV data
        if path and path.split(".")[-1].lower() == "csv":
            opts = options or {}
            self.instance_types = pd.read_csv(filepath_or_buffer=path, parse_dates=["availability_start", "availability_end"], **opts)

        elif instance_types:
            self.instance_types = pd.DataFrame(data=[i.to_dict() for i in instance_types] if instance_types else [],
                                               columns=["instance_type_id", "market_space", "capacity", "price", "availability_start", "availability_end"])
        else:
            raise ValueError("Can only process CSV data.")

        self.instance_types = self.instance_types.astype(dtype={"availability_start": pd.DatetimeTZDtype(tz=default_tz),
                                                                "availability_end": pd.DatetimeTZDtype(tz=default_tz)})

        if not self.instance_types.empty:
            self.instance_types = self.instance_types.set_index("instance_type_id", drop=False)

    def fits_type(self, application: Union[str, Application], instance_type: Union[str, InstanceType]) -> bool:
        if isinstance(application, str):
            application = self.applications.loc[application]

        if isinstance(instance_type, str):
            instance_type = self.instance_types.loc[instance_type]

        prob = st.norm.cdf(round(((instance_type.capacity - application.res_demand) / (application.res_demand_deviation or 1)), 5))
        if (application.res_demand_deviation != 0 and prob < self.min_quality_of_service) or \
                (application.res_demand_deviation == 0 and round(instance_type.capacity - application.res_demand, 2) < 0) or \
                (instance_type.market_space == "reserved" and (application.starting_time < instance_type.availability_start
                                                               or application.finishing_time > instance_type.availability_end)):
            return False
        return True

    def optimize(self, **options):
        pass
