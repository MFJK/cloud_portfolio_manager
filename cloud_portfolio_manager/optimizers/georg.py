import uuid
import random

import scipy.stats as st
import numpy as np

from multiprocessing import Pool

from cloud_portfolio_manager.optimizers import PortfolioOptimizer
from cloud_portfolio_manager.portfolio import Portfolio
from cloud_portfolio_manager.models import Application, Instance, InstanceType


class Georg(PortfolioOptimizer):

    # Genetic optimization of resource groupings
    def optimize(self,
                 population_size,
                 number_of_generations,
                 max_generations_without_improvements,
                 convergence_threshold,
                 parent_selection_strategy,
                 number_of_offspring,
                 mutation_rate,
                 number_of_mutations,
                 survivor_selection_strategy):

        # Don't use a context manager to avoid unnecessary overhead with re-instantiations
        pool = Pool()
        terminate = False
        generation_number = 0
        highest_fitness = None
        generations_without_improvements = 0
        generations = []

        population = pool.starmap(self.initialize_individual, zip([self.applications] * population_size,
                                                                  [self.instance_types] * population_size,
                                                                  [self.time_unit] * population_size))
        self.logger.info(f"Finished population initialization.")
        population = sorted(population, key=lambda individual: individual.calculate_cost())
        generations.append(population)

        while not terminate:
            # Each couple produces one offspring
            parents = self.select_parents(population=population, strategy=parent_selection_strategy, number_of_parents=number_of_offspring)

            offspring = pool.starmap(self.create_offspring, zip(parents, [mutation_rate] * len(parents), [number_of_mutations] * len(parents)))

            population = self.select_survivors(current_generation=population, offspring=offspring, strategy=survivor_selection_strategy)
            population = sorted(population, key=lambda individual: individual.calculate_cost())

            generation_number += 1
            generations.append(population)

            average_fitness = sum(i.calculate_cost() for i in population) / len(population)
            gen_highest_fitness = population[0].calculate_cost()

            self.logger.info(f"Generation {generation_number}\n"
                             f"Highest fitness: {gen_highest_fitness}\n"
                             f"Average fitness: {average_fitness}")

            if gen_highest_fitness == highest_fitness:
                generations_without_improvements += 1
            else:
                highest_fitness = gen_highest_fitness

            # Evaluate termination criteria
            if max_generations_without_improvements is not None and generations_without_improvements >= max_generations_without_improvements:
                terminate = True
            elif convergence_threshold is not None and abs(gen_highest_fitness - average_fitness) <= convergence_threshold:
                terminate = True
            elif generation_number >= number_of_generations:
                terminate = True

        pool.close()
        pool.join()

        return sorted(population, key=lambda individual: individual.calculate_cost()), generations

    def create_offspring(self, parents, mutation_rate, number_of_mutations):
        offspring = self.crossover(primary=parents[0], secondary=parents[1])

        if round(random.uniform(0, 1), 2) < mutation_rate:
            offspring = self.mutate(individual=offspring, number_of_mutations=number_of_mutations)

        return offspring

    @staticmethod
    def select_survivors(current_generation, offspring, strategy):
        if strategy == "fittest_current":
            current_generation = sorted(current_generation, key=lambda individual: individual.calculate_cost())
            new_generation = current_generation[:-len(offspring)] + offspring

        elif strategy == "fittest_overall":
            new_generation = sorted(current_generation + offspring, key=lambda individual: individual.calculate_cost())
            new_generation = new_generation[:len(current_generation)]

        else:
            raise ValueError(f"Unknown survivor selection strategy: {strategy}")

        return new_generation

    @staticmethod
    def select_parents(population, strategy, number_of_parents):
        if strategy == "roulette_wheel":
            # Based on: https://rocreguant.com/roulette-wheel-selection-python/2019/
            total_fitness = sum((i.calculate_cost() for i in population))
            probabilities = [i.calculate_cost() / total_fitness for i in population]
            couples = [np.random.choice(population, size=2, p=probabilities) for _ in range(number_of_parents)]

        elif strategy == "tournament":
            couples = [[sorted(np.random.choice(population, size=min(len(population), 4)), key=lambda p: p.calculate_cost())[0],
                        sorted(np.random.choice(population, size=min(len(population), 4)), key=lambda p: p.calculate_cost())[0]]
                       for _ in range(number_of_parents)]

        elif strategy == "random":
            couples = [[np.random.choice(population), np.random.choice(population)] for _ in range(number_of_parents)]

        else:
            raise ValueError(f"Unknown parent selection strategy: {strategy}")

        return couples

    def initialize_individual(self, applications, instance_types, time_unit):

        individual = Portfolio(time_unit=time_unit, min_quality_of_service=self.min_quality_of_service)
        for application_id, app in applications.iterrows():
            starting_time = app["starting_time"].floor(freq=f"{time_unit[0]}{time_unit[1]}")
            finishing_time = app["finishing_time"].floor(freq=f"{time_unit[0]}{time_unit[1]}")

            assigned = False
            if bool(random.randint(0, 1)):
                candidate_instances = individual.get_instances(from_time=starting_time,
                                                               to_time=finishing_time,
                                                               demand=app["res_demand"],
                                                               deviation=app["res_demand_deviation"])

                if not candidate_instances.empty:
                    individual.add_application(candidate_instances.index.values[0], Application(**app))
                    assigned = True

            if not assigned:
                if app["preemptible"] is False:
                    type_cond = (instance_types["market_space"] == "reserved") | (instance_types["market_space"] == "on_demand")
                else:
                    type_cond = ((instance_types["market_space"] == "reserved") |
                                 (instance_types["market_space"] == "on_demand") |
                                 (instance_types["market_space"] == "spot"))

                probabilities = st.norm.cdf(((instance_types["capacity"] - app["res_demand"]) / (app["res_demand_deviation"] or 1)).astype(float))
                capacity_cond = probabilities >= self.min_quality_of_service
                starting_cond = ((instance_types["availability_start"] <= app["starting_time"]) | instance_types["availability_start"].isnull())
                finishing_cond = ((instance_types["availability_end"] >= app["finishing_time"]) | instance_types["availability_end"].isnull())
                candidate_instances = instance_types[type_cond & capacity_cond & starting_cond & finishing_cond]

                if candidate_instances.empty:
                    raise Exception(f"No fitting instance types for application: {application_id}")

                assigned_instance = candidate_instances.sample().iloc[0]

                new_instance = Instance(instance_id=str(uuid.uuid4()),
                                        beginning_time=starting_time if assigned_instance["market_space"] != "reserved" else None,
                                        ending_time=finishing_time if assigned_instance["market_space"] != "reserved" else None,
                                        instance_type=InstanceType(instance_type_id=assigned_instance["instance_type_id"],
                                                                   capacity=assigned_instance["capacity"],
                                                                   market_space=assigned_instance["market_space"],
                                                                   price=assigned_instance["price"],
                                                                   availability_start=assigned_instance["availability_start"],
                                                                   availability_end=assigned_instance["availability_end"]))

                individual.add_instance(new_instance)
                individual.add_application(new_instance.instance_id, Application(**app))

        return individual

    def crossover(self, primary, secondary):
        self.logger.info(f"Performing crossover between {primary.portfolio_id} and {secondary.portfolio_id}")

        offspring = Portfolio(time_unit=self.time_unit, min_quality_of_service=self.min_quality_of_service)
        free_preemptible_apps = set()
        free_non_preemptible_apps = set()
        primary_ignore = set()
        secondary_ignore = set()

        for primary_group, secondary_group in zip(primary.packing_pattern.groupby("time_step"), secondary.packing_pattern.groupby("time_step")):

            primary_active = primary_group[1].index.unique(level=1).difference(primary_ignore)
            secondary_active = secondary_group[1].index.unique(level=1).difference(secondary_ignore)

            if primary_active.empty and secondary_active.empty:
                continue

            primary_instances = primary.get_instances(instance_ids=primary_active)
            secondary_instances = secondary.get_instances(instance_ids=secondary_active)

            if not primary_instances.empty:
                primary_instances = primary_instances.sort_values(["utilization_efficiency"], ascending=False)

            if not secondary_instances.empty:
                secondary_instances = secondary_instances.sort_values(["utilization_efficiency"], ascending=False)

            lower_bound = min(len(primary_instances), len(secondary_instances))
            upper_bound = max(len(primary_instances), len(secondary_instances))

            partial_solution = []
            # Perform zip merge on instances, if both parents have instances assigned to them for this particular timeslot
            if not primary_instances.empty and not secondary_instances.empty:
                for idx in range(lower_bound):
                    # Inherit instance with better cost utilization rate first
                    if primary_instances.iloc[idx]["utilization_efficiency"] >= secondary_instances.iloc[idx]["utilization_efficiency"]:
                        partial_solution.extend([("primary", primary_instances.iloc[idx]["instance_id"]),
                                                 ("secondary", secondary_instances.iloc[idx]["instance_id"])])
                    else:
                        partial_solution.extend([("secondary", secondary_instances.iloc[idx]["instance_id"]),
                                                 ("primary", primary_instances.iloc[idx]["instance_id"])])

            # Inherit all remaining instances, in case one chromosome was longer
            if len(primary_instances) > len(secondary_instances):
                partial_solution.extend(list(map(lambda i: ("primary", i), list(primary_instances.iloc[(lower_bound - 1):upper_bound].index))))
            elif len(primary_instances) < len(secondary_instances):
                partial_solution.extend(list(map(lambda i: ("secondary", i), list(secondary_instances.iloc[(lower_bound - 1):upper_bound].index))))

            # Iterate over the instances in the partial solution
            for parent, instance_id in partial_solution:
                # For each instance in the partial solution, get all applications assigned to it, from the the respective parent
                if parent == "primary":
                    applications = primary.get_applications(instance_ids=[instance_id])
                else:
                    applications = secondary.get_applications(instance_ids=[instance_id])

                # Instead of iterating over each application and performing an individual check, we can check the assignment status of all apps at once.
                assigned_applications = offspring.get_packing_pattern(application_ids=applications.index, from_time=primary_group[0])

                # Ignore the instance if:
                #   - Any app that is assigned to the parent for this instance, has already been assigned for timestamps greater than the current one
                #   - The same instance (with a different packing pattern) has already been inherited from another parent
                if not assigned_applications.empty or not offspring.get_instances(instance_ids=[instance_id]).empty:
                    # If we find that some of the applications are already assigned, then we can remove the instance
                    if parent == "primary":
                        primary_ignore.add(instance_id)
                    else:
                        secondary_ignore.add(instance_id)

                    assigned_non_preemptible = set(applications.loc[(applications.index.isin(assigned_applications.index.unique(level=2))) &
                                                                    (applications["preemptible"] == False)].index)

                    assigned_preemptible = set(applications.loc[(applications.index.isin(assigned_applications.index.unique(level=2))) &
                                                                (applications["preemptible"] == True)].index)

                    # Keep track of all non-assigned non-preemptible applications that might need to be reinserted later on
                    free_non_preemptible_apps.update(set(applications[applications["preemptible"] == False].index) - assigned_non_preemptible)

                    # Keep track of all non-assigned preemptible applications, for which we later need to find gaps
                    free_preemptible_apps.update(set(applications[applications["preemptible"] == True].index) - assigned_preemptible)

                else:
                    # Add instance to offspring
                    if parent == "primary":
                        instance = primary_instances.loc[instance_id].copy()
                        primary_ignore.add(instance_id)
                    else:
                        instance = secondary_instances.loc[instance_id].copy()
                        secondary_ignore.add(instance_id)

                    offspring.add_instance(instance)

                    # Add all applications to the new instance
                    for application_id, application in applications.iterrows():

                        if parent == "primary":
                            app_packing_pattern = primary.get_packing_pattern(instance_ids=[instance_id], application_ids=[application_id])
                        else:
                            app_packing_pattern = secondary.get_packing_pattern(instance_ids=[instance_id], application_ids=[application_id])

                        offspring.bulk_add_application(application=application,
                                                       time_steps=app_packing_pattern.index.get_level_values(0),
                                                       instance_ids=app_packing_pattern.index.get_level_values(1))

        # Insert all free applications with a freely definable heuristic
        # Apps that have been marked as free might have been assigned in the mean time, if the host instance was inherited from the other parent
        unassigned_non_preemptible_apps = free_non_preemptible_apps - set(offspring.applications.index)

        # Check for gaps for all preemptible apps
        free_preemptible_apps = primary.get_applications(application_ids=free_preemptible_apps)
        reinsert_preemptible = {}
        for application_id, application in free_preemptible_apps.iterrows():
            gaps = offspring.get_assignment_gaps(application=application)

            if gaps:
                reinsert_preemptible[application_id] = {
                    "application": application,
                    "gaps": gaps
                }

        if unassigned_non_preemptible_apps or reinsert_preemptible:
            # Since the applications are the same in primary and secondary, it does not matter which is used to retrieve the data.
            reinsert_non_preemptible = primary.get_applications(application_ids=unassigned_non_preemptible_apps).copy()

            # Add remaining free applications with an insertion heuristic
            offspring = self.insertion_heuristic(non_preemptible_applications=reinsert_non_preemptible,
                                                 preemptible_applications=reinsert_preemptible,
                                                 portfolio=offspring)

        return offspring

    def insertion_heuristic(self, non_preemptible_applications, preemptible_applications, portfolio):

        # For non-preemptible applications there cannot be multiple assignment gaps. Therefore, we can treat them as a single gap.
        for application_id, application in non_preemptible_applications.iterrows():
            starting_time = application["starting_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")
            finishing_time = application["finishing_time"].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")

            candidate_instances = portfolio.get_instances(from_time=starting_time,
                                                          to_time=finishing_time,
                                                          demand=application["res_demand"],
                                                          deviation=application["res_demand_deviation"])

            if not candidate_instances.empty:
                portfolio.add_application(candidate_instances.index.values[0], Application(**application))

            else:

                market_spaces = ["reserved", "on_demand"] if not application["preemptible"] else ["reserved", "on_demand", "spot"]
                probabilities = st.norm.cdf(
                    ((self.instance_types["capacity"] - application["res_demand"]) / (application["res_demand_deviation"] or 1)).astype(float))

                conditions = [probabilities >= self.min_quality_of_service,
                              self.instance_types["market_space"].isin(market_spaces),
                              ((self.instance_types["availability_start"] <= starting_time) | self.instance_types["availability_start"].isnull()),
                              ((self.instance_types["availability_end"] >= finishing_time) | self.instance_types["availability_end"].isnull())]

                instances = self.instance_types.loc[np.logical_and.reduce(conditions)]

                new_instance_type = instances.sample().iloc[0]
                new_instance = Instance(instance_id=str(uuid.uuid4()),
                                        beginning_time=starting_time if new_instance_type["market_space"] != "reserved" else None,
                                        ending_time=finishing_time if new_instance_type["market_space"] != "reserved" else None,
                                        instance_type=InstanceType(instance_type_id=new_instance_type["instance_type_id"],
                                                                   capacity=new_instance_type["capacity"],
                                                                   market_space=new_instance_type["market_space"],
                                                                   price=new_instance_type["price"],
                                                                   availability_start=new_instance_type["availability_start"],
                                                                   availability_end=new_instance_type["availability_end"]))

                portfolio.add_instance(new_instance)
                portfolio.add_application(new_instance.instance_id, Application(**application))

        for application_id in preemptible_applications.keys():
            application = preemptible_applications[application_id]["application"]

            # We have already precalculated all gaps in the crossover/mutation operator
            for index, gap in preemptible_applications[application_id]["gaps"]:
                starting_time = gap.index[0].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")
                finishing_time = gap.index[-1].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")

                self.logger.info(f"Reinserting application {application_id} for gap {starting_time} - {finishing_time}")

                candidate_instances = portfolio.get_instances(from_time=starting_time,
                                                              to_time=finishing_time,
                                                              demand=application["res_demand"],
                                                              deviation=application["res_demand_deviation"])

                if not candidate_instances.empty:
                    portfolio.add_application(candidate_instances.index.values[0], Application(**application), from_time=starting_time, to_time=finishing_time)

                else:
                    market_spaces = ["reserved", "on_demand"] if not application["preemptible"] else ["reserved", "on_demand", "spot"]
                    probabilities = st.norm.cdf(
                        ((self.instance_types["capacity"] - application["res_demand"]) / (application["res_demand_deviation"] or 1)).astype(float))

                    conditions = [probabilities >= self.min_quality_of_service,
                                  self.instance_types["market_space"].isin(market_spaces),
                                  ((self.instance_types["availability_start"] <= starting_time) | self.instance_types["availability_start"].isnull()),
                                  ((self.instance_types["availability_end"] >= finishing_time) | self.instance_types["availability_end"].isnull())]

                    instances = self.instance_types.loc[np.logical_and.reduce(conditions)]

                    new_instance_type = instances.sample().iloc[0]
                    new_instance = Instance(instance_id=str(uuid.uuid4()),
                                            beginning_time=starting_time if new_instance_type["market_space"] != "reserved" else None,
                                            ending_time=finishing_time if new_instance_type["market_space"] != "reserved" else None,
                                            instance_type=InstanceType(instance_type_id=new_instance_type["instance_type_id"],
                                                                       capacity=new_instance_type["capacity"],
                                                                       market_space=new_instance_type["market_space"],
                                                                       price=new_instance_type["price"],
                                                                       availability_start=new_instance_type["availability_start"],
                                                                       availability_end=new_instance_type["availability_end"]))

                    portfolio.add_instance(new_instance)
                    portfolio.add_application(new_instance.instance_id, Application(**application), from_time=starting_time, to_time=finishing_time)

        return portfolio

    def mutate(self, individual, number_of_mutations):
        self.logger.info(f"Performing mutation on {individual.portfolio_id}")

        unassigned_non_preemptible = set()
        unassigned_preemptible = {}

        # Select random instances to remove
        instances_to_remove = individual.instances.sample(n=min(number_of_mutations, len(individual.instances)))

        # Keep track of the applications that lost their host
        #  - non-preemptible applications no longer have a host at all
        #  - preemptible applications might only have lost their assignment partially
        free_applications = individual.get_applications(instance_ids=instances_to_remove.index.values)

        # Remove the randomly selected instances for good
        individual.delete_instances(instance_ids=instances_to_remove.index)

        for application_id, application in free_applications.iterrows():

            gaps = individual.get_assignment_gaps(application=application)

            for index, gap in gaps:
                assigned = False
                starting_time = gap.index[0].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")
                finishing_time = gap.index[-1].floor(freq=f"{self.time_unit[0]}{self.time_unit[1]}")

                candidate_instances = individual.get_instances(from_time=starting_time, to_time=finishing_time)

                if candidate_instances.empty:
                    if application["preemptible"]:
                        unassigned_preemptible[application_id] = application
                    else:
                        unassigned_non_preemptible.add(application_id)

                    continue

                for instance_id, instance in candidate_instances.iterrows():
                    applications = individual.get_applications(instance_ids=[instance_id])

                    if len(applications) == 1:
                        if individual.dominates(instance_id, application_id, applications.index.values, starting_time, finishing_time):

                            for app_id, app in applications.iterrows():
                                if app["preemptible"]:
                                    unassigned_preemptible[app_id] = app
                                else:
                                    unassigned_non_preemptible.add(app_id)

                            individual.delete_applications(application_ids=applications.index.values, from_store=False, from_packing_pattern=True,
                                                           from_time=starting_time, to_time=finishing_time)
                            individual.add_application(instance_id=instance_id, application=application, from_time=starting_time, to_time=finishing_time)
                            assigned = True
                            break

                    else:

                        for app_1, app_2 in zip(applications.index[0:len(applications.index) - 1], applications.index[1:len(applications.index)]):
                            if individual.dominates(instance_id, application_id, [app_1, app_2], starting_time, finishing_time):
                                for app_id in (app_1, app_2):
                                    app = applications.loc[app_id]
                                    if application["preemptible"]:
                                        unassigned_preemptible[app_id] = app
                                    else:
                                        unassigned_non_preemptible.add(app_id)

                                individual.delete_applications(application_ids=[app_1, app_2], from_store=False, from_packing_pattern=True,
                                                               from_time=starting_time, to_time=finishing_time)
                                individual.add_application(instance_id=instance_id, application=application, from_time=starting_time, to_time=finishing_time)
                                assigned = True
                                break

                    if assigned is True:
                        break

                if not assigned:
                    if application["preemptible"]:
                        unassigned_preemptible[application_id] = application
                    else:
                        unassigned_non_preemptible.add(application_id)

        if unassigned_non_preemptible or unassigned_preemptible:
            reinsert_non_preemptible = individual.get_applications(application_ids=unassigned_non_preemptible)

            reinsert_preemptible = {}
            for application_id, application in unassigned_preemptible.items():
                gaps = individual.get_assignment_gaps(application=application)

                if gaps:
                    reinsert_preemptible[application_id] = {
                        "application": application,
                        "gaps": gaps
                    }

            self.insertion_heuristic(non_preemptible_applications=reinsert_non_preemptible,
                                     preemptible_applications=reinsert_preemptible,
                                     portfolio=individual)

        return individual
