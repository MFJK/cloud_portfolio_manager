import uuid

import pandas as pd

from pathlib import Path

from cloud_portfolio_manager.models import InstanceType, Application
from cloud_portfolio_manager.optimizers import Erich


def test_one_non_preemptible_application_is_assigned_to_one_reserved_instance():
    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.1,
                    preemptible=False)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=2,
                     price=100,
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z"))
    ]

    optimizer = Erich(applications=applications, instance_types=instance_types)
    portfolio = optimizer.optimize()
    instances = portfolio.get_instances()

    assert len(instances) == 1
    assert any(instance_types[0].instance_type_id == instances.instance_type_id)
    assert any(instance_types[0].availability_start == instances.availability_start)
    assert any(instance_types[0].availability_end == instances.availability_end)


def test_one_non_preemptible_application_is_reassigned_to_one_on_demand_instance():
    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.1,
                    preemptible=False)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=2,
                     price=100,  # Reserved instances have a total price
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=2,
                     price=0.5)  # This is the rate per time slot
    ]

    optimizer = Erich(applications=applications, instance_types=instance_types)
    portfolio = optimizer.optimize()
    instances = portfolio.get_instances()

    assert len(instances) == 1
    assert any(instance_types[1].instance_type_id == instances.instance_type_id)
    assert any(applications[0].starting_time == instances.beginning_time)
    assert any(applications[0].finishing_time == instances.ending_time)


def test_one_preemptible_application_is_assigned_to_one_spot_instance():
    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-13T23:30:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.1,
                    preemptible=True)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="spot",
                     capacity=2,
                     price=100)
    ]

    options = {"time_unit": (1, "H")}
    optimizer = Erich(applications=applications, instance_types=instance_types)
    portfolio = optimizer.optimize()
    instances = portfolio.get_instances()

    assert len(instances) == 1
    assert any(instance_types[0].instance_type_id == instances.instance_type_id)
    assert any(applications[0].starting_time == instances.beginning_time)
    assert any(applications[0].finishing_time.floor(freq=f"{options['time_unit'][0]}{options['time_unit'][1]}") == instances.ending_time)


def test_application_res_demand_variance_exceeds_capacity_leading_to_multiple_instance_reservations():
    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=0.5,
                    res_demand_deviation=0.5,
                    preemptible=False)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=2,
                     price=100,
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z"))
    ]

    optimizer = Erich(applications=applications, instance_types=instance_types)
    portfolio = optimizer.optimize()
    instances = portfolio.get_instances()

    assert len(instances) == 2


def test_one_of_two_reserved_instances_is_reallocated_to_on_demand():
    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-31T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.1,
                    preemptible=False),
        # Since this application is only running for a single day, the additional reserved instance needed for this app is economically inefficient.
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
                    res_demand=0.5,
                    res_demand_deviation=0.5,
                    preemptible=False)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=2,
                     price=100,
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=2,
                     price=0.2)
    ]

    optimizer = Erich(applications=applications, instance_types=instance_types)
    portfolio = optimizer.optimize()
    instances = portfolio.get_instances()

    assert len(instances) == 2
    assert any(instance_types[0].instance_type_id == instances.instance_type_id)
    assert any(instance_types[1].instance_type_id == instances.instance_type_id)


def test_preemptible_application_is_assigned_to_reserved_instance():
    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-30T23:00:00Z"),
                    res_demand=0.5,
                    res_demand_deviation=0.1,
                    preemptible=True)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=2,
                     price=100,
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="spot",
                     capacity=2,
                     price=0.1)
    ]

    optimizer = Erich(applications=applications, instance_types=instance_types)
    portfolio = optimizer.optimize()
    instances = portfolio.get_instances()

    assert len(instances) == 1
    assert any(instance_types[0].instance_type_id == instances.instance_type_id)


def test_erich_assignment_validity():
    applications = [
        Application(application_id="a1",
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=3,
                    res_demand_deviation=0.5,
                    preemptible=False),
        Application(application_id="a2",
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=4,
                    res_demand_deviation=0.7,
                    preemptible=False),
        Application(application_id="a3",
                    starting_time=pd.Timestamp("2021-07-06T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-20T00:00:00Z"),
                    res_demand=1.2,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id="a4",
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-25T00:00:00Z"),
                    res_demand=8,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id="a5",
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-30T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.2,
                    preemptible=True),
        Application(application_id="a6",
                    starting_time=pd.Timestamp("2021-07-03T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-20T00:00:00Z"),
                    res_demand=10,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id="a7",
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-30T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.2,
                    preemptible=True)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=15,
                     price=200 * 720,  # reserved instances have a total price
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=6,
                     price=100 * 504,
                     availability_start=pd.Timestamp("2021-07-10T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=5,
                     price=300),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=2.5,
                     price=250),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="spot",
                     capacity=5,
                     price=60)
    ]

    optimizer = Erich(applications=applications, instance_types=instance_types, time_unit=(1, "H"))
    portfolio = optimizer.optimize()

    status = portfolio.check_application_assignment_validity()

    assert status["number_of_applications"] == 7
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"


def test_erich_spot_allocation_does_not_assign_duplicate_time_slots():
    app_path = Path(__file__).parent.joinpath("test_data/applications_duplicate_test.csv")
    instance_path = Path(__file__).parent.joinpath("test_data/instance_types_duplicate_test.csv")

    optimizer = Erich()
    optimizer.load_applications(str(app_path))
    optimizer.load_instance_types(str(instance_path))

    portfolio = optimizer.optimize()

    status = portfolio.check_application_assignment_validity()

    assert status["number_of_applications"] == 20
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"
