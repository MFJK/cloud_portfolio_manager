from pathlib import Path

import pandas as pd

from cloud_portfolio_manager.models.instance_type import InstanceType
from cloud_portfolio_manager.models.application import Application

from cloud_portfolio_manager.optimizers import PortfolioOptimizer


def test_csv_load_function_for_portfolio():
    optimizer = PortfolioOptimizer()

    assert optimizer.applications.empty
    assert optimizer.instance_types.empty

    data_path = Path(__file__).parent.joinpath("test_data")
    optimizer.load_applications(path=str(data_path.joinpath("applications_1.CSV")))
    optimizer.load_instance_types(path=str(data_path.joinpath("instance_types_1.CSV")))

    assert not optimizer.applications.empty
    assert not optimizer.instance_types.empty


def test_type_fit_check():
    application_1 = Application(application_id="a1",
                                starting_time=pd.Timestamp("2021-11-16T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-11-16T10:00:00Z"),
                                preemptible=False,
                                res_demand=5,
                                res_demand_deviation=2)

    application_2 = Application(application_id="a2",
                                starting_time=pd.Timestamp("2021-11-16T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-11-16T10:00:00Z"),
                                preemptible=False,
                                res_demand=9,
                                res_demand_deviation=0)

    application_3 = Application(application_id="a3",
                                starting_time=pd.Timestamp("2021-11-16T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-11-16T10:00:00Z"),
                                preemptible=False,
                                res_demand=9,
                                res_demand_deviation=2)

    instance_type_1 = InstanceType(instance_type_id="i1",
                                   market_space="reserved",
                                   availability_start=pd.Timestamp("2021-11-16T00:00:00Z"),
                                   availability_end=pd.Timestamp("2021-11-16T23:00:00Z"),
                                   capacity=10,
                                   price=100)

    instance_type_2 = InstanceType(instance_type_id="i2",
                                   market_space="on_demand",
                                   capacity=7.2,
                                   price=100)

    instance_type_3 = InstanceType(instance_type_id="i1",
                                   market_space="reserved",
                                   availability_start=pd.Timestamp("2000-01-01T00:00:00Z"),
                                   availability_end=pd.Timestamp("2000-01-01T23:00:00Z"),
                                   capacity=10,
                                   price=100)

    optimizer = PortfolioOptimizer()

    assert optimizer.fits_type(application_1, instance_type_1)
    # Probability that the expected demand exceeds the capacity is too high
    assert not optimizer.fits_type(application_1, instance_type_2)
    # Availability times do not match
    assert not optimizer.fits_type(application_1, instance_type_3)
    # Variance is 0 and mean demand is less than capacity
    assert optimizer.fits_type(application_2, instance_type_1)
    # Variance is 0 but mean demand is greater than capacity
    assert not optimizer.fits_type(application_2, instance_type_2)
    # Variance is not 0 but mean is very close to capacity and probability does not stay within limits
    assert not optimizer.fits_type(application_3, instance_type_1)
