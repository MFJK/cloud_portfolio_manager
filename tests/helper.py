import numpy as np
import pandas as pd


def generate_random_dates(start, end, n=1, tz="UTC"):
    start_u = start.value//10**9
    end_u = end.value//10**9

    return [pd.Timestamp(date, tz=tz) for date in (10**9*np.random.randint(start_u, end_u, n, dtype=np.int64)).view("M8[ns]")]
