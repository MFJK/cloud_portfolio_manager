import uuid

import numpy as np
import pandas as pd

from cloud_portfolio_manager.models import InstanceType, Instance, Application
from cloud_portfolio_manager.portfolio import Portfolio


def test_instance_utilization_rate_is_calculated_correctly():
    """
    This test ensures that the total utilization as well as the average is correctly calculated when new applications are added or removed
    """

    application_1 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
                                res_demand=8,
                                res_demand_deviation=2,
                                preemptible=False)

    application_2 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-07-01T11:00:00Z"),
                                res_demand=10,
                                res_demand_deviation=0,
                                preemptible=False)

    instance_a = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=20,
                                                     market_space="reserved",
                                                     price=240,
                                                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-07-01T23:00:00Z")))

    portfolio = Portfolio(time_unit=(1, "H"))
    portfolio.add_instance(instance=instance_a)
    portfolio.add_application(instance_id=instance_a.instance_id, application=application_1)

    application_1_utilization = application_1.res_demand * (
            1 + (application_1.finishing_time - application_1.starting_time) / pd.Timedelta(value=portfolio.time_unit[0], unit=portfolio.time_unit[1]))

    assert portfolio.instances.loc[instance_a.instance_id, "utilization_total"] == application_1_utilization
    assert portfolio.instances.loc[instance_a.instance_id, "utilization_rate"] == 0.4
    assert portfolio.instances.loc[instance_a.instance_id, "utilization_efficiency"] == 0.4 / 10

    portfolio.add_application(instance_id=instance_a.instance_id, application=application_2)

    application_2_utilization = application_2.res_demand * (
            1 + (application_2.finishing_time - application_2.starting_time) / pd.Timedelta(value=portfolio.time_unit[0], unit=portfolio.time_unit[1]))

    assert portfolio.instances.loc[instance_a.instance_id, "utilization_total"] == application_1_utilization + application_2_utilization
    assert portfolio.instances.loc[instance_a.instance_id, "utilization_rate"] == 0.65
    assert portfolio.instances.loc[instance_a.instance_id, "utilization_efficiency"] == 0.65 / 10

    portfolio.delete_applications(application_ids=[application_1.application_id], from_store=False, from_packing_pattern=True)

    assert portfolio.instances.loc[instance_a.instance_id, "utilization_total"] == application_2_utilization
    assert portfolio.instances.loc[instance_a.instance_id, "utilization_rate"] == 0.25
    assert portfolio.instances.loc[instance_a.instance_id, "utilization_efficiency"] == 0.25 / 10


def test_portfolio_stats():
    """
    This test ensures that the portfolio stats are calculated correctly.
    """

    portfolio = Portfolio()

    instances = [
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                            capacity=20,
                                            market_space="reserved",
                                            price=240,
                                            availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                                            availability_end=pd.Timestamp("2021-07-01T23:00:00Z"))),
        Instance(instance_id=str(uuid.uuid4()),
                 beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
                 instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                            capacity=20,
                                            market_space="on_demand",
                                            # => cost = 10 * 24
                                            price=10)),
        Instance(instance_id=str(uuid.uuid4()),
                 beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
                 instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                            capacity=20,
                                            market_space="on_demand",
                                            # => cost = 20 * 24
                                            price=20)),
        Instance(instance_id=str(uuid.uuid4()),
                 beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
                 instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                            capacity=20,
                                            market_space="spot",
                                            price=5))
    ]

    for instance in instances:
        portfolio.add_instance(instance)

    stats = portfolio.get_stats()

    assert stats["number_of_hosts_overall"] == 4
    assert stats["number_of_hosts_reserved"] == 1
    assert stats["number_of_hosts_on_demand"] == 2
    assert stats["number_of_hosts_spot"] == 1
    assert stats["mean_utilization_overall"] == 0
    assert stats["mean_utilization_reserved"] == 0
    assert stats["mean_utilization_on_demand"] == 0
    assert stats["mean_utilization_spot"] == 0
    assert stats["weighted_utilization_overall"] == 0
    assert stats["weighted_utilization_reserved"] == 0
    assert stats["weighted_utilization_on_demand"] == 0
    assert stats["weighted_utilization_spot"] == 0
    assert stats["cost_overall"] == 240 + 10 * 24 + 20 * 24 + 5 * 24

    application_1 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
                                res_demand=8,
                                res_demand_deviation=2,
                                preemptible=False)

    application_2 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-07-01T11:00:00Z"),
                                res_demand=4,
                                res_demand_deviation=1,
                                preemptible=False)

    application_3 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-07-01T11:00:00Z"),
                                res_demand=4,
                                res_demand_deviation=1,
                                preemptible=False)

    portfolio.add_application(instance_id=instances[0].instance_id, application=application_1)
    portfolio.add_application(instance_id=instances[1].instance_id, application=application_2)
    portfolio.add_application(instance_id=instances[2].instance_id, application=application_3)

    stats = portfolio.get_stats()

    assert stats["number_of_hosts_overall"] == 4
    assert stats["number_of_hosts_reserved"] == 1
    assert stats["number_of_hosts_on_demand"] == 2
    assert stats["number_of_hosts_spot"] == 1
    assert stats["mean_utilization_overall"] == 0.15
    assert stats["mean_utilization_reserved"] == 0.4
    assert stats["mean_utilization_on_demand"] == 0.1
    assert stats["mean_utilization_spot"] == 0
    assert stats["weighted_utilization_overall"] == 0.15
    assert stats["weighted_utilization_reserved"] == 0.4
    assert stats["weighted_utilization_on_demand"] == 0.1
    assert stats["weighted_utilization_spot"] == 0
    assert stats["cost_overall"] == 240 + 10 * 24 + 20 * 24 + 5 * 24

    portfolio.delete_applications(application_ids=[application_3.application_id])

    stats = portfolio.get_stats()

    assert stats["number_of_hosts_overall"] == 4
    assert stats["number_of_hosts_reserved"] == 1
    assert stats["number_of_hosts_on_demand"] == 2
    assert stats["number_of_hosts_spot"] == 1
    assert stats["mean_utilization_overall"] == 0.125
    assert stats["mean_utilization_reserved"] == 0.4
    assert stats["mean_utilization_on_demand"] == 0.05
    assert stats["mean_utilization_spot"] == 0
    assert stats["weighted_utilization_overall"] == 0.125
    assert stats["weighted_utilization_reserved"] == 0.4
    assert stats["weighted_utilization_on_demand"] == 0.05
    assert stats["weighted_utilization_spot"] == 0
    assert stats["cost_overall"] == 240 + 10 * 24 + 20 * 24 + 5 * 24

    portfolio.delete_instances(instance_ids=[instances[3].instance_id])

    stats = portfolio.get_stats()

    assert stats["number_of_hosts_overall"] == 3
    assert stats["number_of_hosts_reserved"] == 1
    assert stats["number_of_hosts_on_demand"] == 2
    assert stats["number_of_hosts_spot"] == 0
    assert round(stats["mean_utilization_overall"], 4) == 0.1667
    assert stats["mean_utilization_reserved"] == 0.4
    assert stats["mean_utilization_on_demand"] == 0.05
    assert np.isnan(stats["mean_utilization_spot"])
    assert round(stats["mean_utilization_overall"], 4) == 0.1667
    assert stats["weighted_utilization_reserved"] == 0.4
    assert stats["weighted_utilization_on_demand"] == 0.05
    assert np.isnan(stats["weighted_utilization_spot"])
    assert stats["cost_overall"] == 240 + 10 * 24 + 20 * 24


def test_portfolio_instance_retrieval():
    portfolio = Portfolio()

    instance_1 = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=20,
                                                     market_space="reserved",
                                                     price=240,
                                                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-07-01T23:00:00Z")))

    instance_2 = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=20,
                                                     market_space="reserved",
                                                     price=240,
                                                     availability_start=pd.Timestamp("2021-06-01T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-07-03T23:00:00Z")))

    instance_3 = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=50,
                                                     market_space="reserved",
                                                     price=240,
                                                     availability_start=pd.Timestamp("2021-07-03T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-08-01T23:00:00Z")))

    instance_4 = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=20,
                                                     market_space="reserved",
                                                     price=240,
                                                     availability_start=pd.Timestamp("2021-09-01T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-09-01T10:00:00Z")))

    portfolio.add_instance(instance_1)
    portfolio.add_instance(instance_2)
    portfolio.add_instance(instance_3)
    portfolio.add_instance(instance_4)
    retrieved = portfolio.get_instances(from_time=pd.Timestamp("2021-07-01T00:00:00Z"), to_time=pd.Timestamp("2021-07-01T23:00:00Z"))

    assert len(retrieved) == 2
    assert any(instance_1.instance_id == retrieved.instance_id)
    assert any(instance_2.instance_id == retrieved.instance_id)

    retrieved = portfolio.get_instances(from_time=pd.Timestamp("2021-07-01T05:00:00Z"), to_time=pd.Timestamp("2021-07-03T10:00:00Z"), full_cover=False)
    assert len(retrieved) == 3
    assert any(instance_1.instance_id == retrieved.instance_id)
    assert any(instance_2.instance_id == retrieved.instance_id)
    assert any(instance_3.instance_id == retrieved.instance_id)

    retrieved = portfolio.get_instances(from_time=pd.Timestamp("2021-07-01T05:00:00Z"), to_time=pd.Timestamp("2021-07-03T10:00:00Z"), full_cover=False,
                                        demand=30, deviation=5)
    assert len(retrieved) == 1
    assert any(instance_3.instance_id == retrieved.instance_id)

    retrieved = portfolio.get_instances(from_time=pd.Timestamp("2021-07-01T05:00:00Z"), to_time=pd.Timestamp("2021-07-03T10:00:00Z"), full_cover=True,
                                        demand=10, deviation=0)
    assert len(retrieved) == 1
    assert any(instance_2.instance_id == retrieved.instance_id)

    retrieved = portfolio.get_instances(from_time=pd.Timestamp("2021-07-01T05:00:00Z"), to_time=pd.Timestamp("2021-07-03T10:00:00Z"), full_cover=True,
                                        demand=30, deviation=0)
    assert len(retrieved) == 0


def test_delete_application_from_to_time():
    """
    This test ensures that an application may only be deleted for the desired time range.
    """

    application_1 = Application(
        application_id="a1",
        res_demand=5,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        preemptible=True
    )

    application_2 = Application(
        application_id="a2",
        res_demand=9,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        preemptible=False
    )

    type_1 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=10)

    instance_1 = Instance(
        instance_id="i1",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    instance_2 = Instance(
        instance_id="i2",
        beginning_time=pd.Timestamp("2021-07-01T12:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_1
    )

    instance_3 = Instance(
        instance_id="i3",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    individual = Portfolio()
    individual.add_instance(instance_1)
    individual.add_instance(instance_2)
    individual.add_instance(instance_3)
    individual.add_application(instance_1.instance_id, application_1, pd.Timestamp("2021-07-01T00:00:00Z"), pd.Timestamp("2021-07-01T11:00:00Z"))
    individual.add_application(instance_2.instance_id, application_1, pd.Timestamp("2021-07-01T12:00:00Z"), pd.Timestamp("2021-07-01T23:00:00Z"))
    individual.add_application(instance_3.instance_id, application_2, pd.Timestamp("2021-07-01T00:00:00Z"), pd.Timestamp("2021-07-01T11:00:00Z"))

    status = individual.check_application_assignment_validity()
    assert status["number_of_applications"] == 2
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    application_1_utilization = application_1.res_demand * (
            1 + (application_1.finishing_time - application_1.starting_time) / pd.Timedelta(value=individual.time_unit[0], unit=individual.time_unit[1]))

    assert individual.instances.loc[instance_1.instance_id, "utilization_total"] == application_1_utilization / 2  # Because i1 shares hosting with i2 for a1
    assert individual.instances.loc[instance_1.instance_id, "utilization_rate"] == 0.5
    assert individual.instances.loc[instance_1.instance_id, "utilization_efficiency"] == 0.5 / 10

    individual.delete_applications(application_ids=[application_1.application_id],
                                   from_store=False,
                                   from_packing_pattern=True,
                                   from_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                   to_time=pd.Timestamp("2021-07-01T05:00:00Z"))

    # status = individual.check_application_assignment_validity()
    # assert status["number_of_applications"] == 2
    # assert status["within_qos"] == True
    # for key, value in status["applications"].items():
    #     assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
    #     assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    assert len(individual.instances) == 3

    deleted_time_span = individual.get_packing_pattern(instance_ids=["i1"], to_time=pd.Timestamp("2021-07-01T05:00:00Z"))
    assert deleted_time_span.empty

    not_deleted_time_span = individual.get_packing_pattern(instance_ids=["i1"], from_time=pd.Timestamp("2021-07-01T05:00:00Z"))
    assert len(not_deleted_time_span.index.unique(level=2)) == 1 and "a1" in not_deleted_time_span.index.unique(level=2)

    unaffected_instance = individual.get_packing_pattern(instance_ids=["i1"], from_time=pd.Timestamp("2021-07-01T05:00:00Z"))
    assert not unaffected_instance.empty

    no_usage = individual.instance_usage.loc[(individual.instance_usage.index.get_level_values(0) >= pd.Timestamp("2021-07-01T00:00:00Z")) &
                                             (individual.instance_usage.index.get_level_values(0) <= pd.Timestamp("2021-07-01T05:00:00Z")) &
                                             (individual.instance_usage.index.get_level_values(1) == "i1")]
    assert all(no_usage["agg_demand"] == 0) and all(no_usage["agg_variance"] == 0)

    unchanged_usage = individual.instance_usage.loc[(individual.instance_usage.index.get_level_values(0) > pd.Timestamp("2021-07-01T05:00:00Z")) &
                                                    (individual.instance_usage.index.get_level_values(1) == "i1")]
    unchanged_usage = unchanged_usage.round({"agg_demand": 4, "agg_variance": 4})
    assert all(unchanged_usage["agg_demand"] == application_1.res_demand) and all(unchanged_usage["agg_variance"] == application_1.res_demand_deviation ** 2)

    assert individual.instances.loc[instance_1.instance_id, "utilization_total"] == application_1_utilization / 4  # Because half the assignments were delted
    assert individual.instances.loc[instance_1.instance_id, "utilization_rate"] == 0.25
    assert individual.instances.loc[instance_1.instance_id, "utilization_efficiency"] == 0.25 / 10

    assert individual.instances.loc[instance_2.instance_id, "utilization_total"] == application_1_utilization / 2  # Because i1 shares hosting with i2 for a1
    assert individual.instances.loc[instance_2.instance_id, "utilization_rate"] == 0.5
    assert individual.instances.loc[instance_2.instance_id, "utilization_efficiency"] == 0.5 / 10