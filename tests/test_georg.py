import uuid
import pytest
import pickle

import pandas as pd

from pathlib import Path

from cloud_portfolio_manager.models import InstanceType, Instance, Application
from cloud_portfolio_manager.portfolio import Portfolio
from cloud_portfolio_manager.optimizers import Georg


def test_crossover_with_one_instance_per_parent():
    """
    This test evaluates the simplest test case, where both parents have exactly one instance (with one application). The primary parent has the better instance
    and so its setting should be inherited. No free applications have to be inserted.
    """

    application = Application(application_id=str(uuid.uuid4()),
                              starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                              finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                              res_demand=2,
                              res_demand_deviation=0.1,
                              preemptible=False)

    primary_instance = Instance(instance_id=str(uuid.uuid4()),
                                instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                           capacity=5,
                                                           market_space="reserved",
                                                           price=100,
                                                           availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                           availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    primary = Portfolio(time_unit=(1, "H"))
    primary.add_instance(primary_instance)
    primary.add_application(primary_instance.instance_id, application)

    # Higher capacity and higher price should lead to a worse cost per load average than the primary instance
    secondary_instance = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=10,
                                                             market_space="reserved",
                                                             price=200,
                                                             availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    secondary = Portfolio(time_unit=(1, "H"))
    secondary.add_instance(secondary_instance)
    secondary.add_application(secondary_instance.instance_id, application)

    # Provide dummy instance type, as interface requires it
    instance_type = InstanceType(instance_type_id="dummy",
                                 market_space="reserved",
                                 capacity=3,
                                 price=180,
                                 availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                 availability_end=pd.Timestamp("2021-08-31T00:00:00Z"))

    optimizer = Georg([application], [instance_type], (1, "H"))
    offspring = optimizer.crossover(primary, secondary)

    assert len(offspring.applications) == 1
    assert len(offspring.instances) == 1
    assert any(primary_instance.instance_id == offspring.instances.instance_id)


def test_crossover_where_one_chromosome_does_not_have_any_instances():
    """
    This test evaluates a scenario, where one chromosome does not have any instances for a certain timeslot. As a result, the offspring should inherit the
    instances of the parent, that does have instances.
    """
    application_a = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    application_b = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-05T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    primary_instance_a = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=2.5,
                                                             market_space="reserved",
                                                             price=100,
                                                             availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    primary_instance_b = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=2.5,
                                                             market_space="reserved",
                                                             price=50,
                                                             availability_start=pd.Timestamp("2021-08-05T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    primary = Portfolio(time_unit=(1, "H"))
    primary.add_instance(primary_instance_a)
    primary.add_instance(primary_instance_b)
    primary.add_application(primary_instance_a.instance_id, application_a)
    primary.add_application(primary_instance_b.instance_id, application_b)

    secondary_instance_a = Instance(instance_id=str(uuid.uuid4()),
                                    instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                               capacity=5,
                                                               market_space="reserved",
                                                               price=150,
                                                               availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                               availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    secondary = Portfolio(time_unit=(1, "H"))
    secondary.add_instance(secondary_instance_a)
    secondary.add_application(secondary_instance_a.instance_id, application_a)
    secondary.add_application(secondary_instance_a.instance_id, application_b)

    # Provide dummy instance type, as interface requires it
    instance_type = InstanceType(instance_type_id="dummy",
                                 market_space="reserved",
                                 capacity=3,
                                 price=180,
                                 availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                 availability_end=pd.Timestamp("2021-08-31T00:00:00Z"))

    optimizer = Georg([application_a, application_b], [instance_type], (1, "H"))
    offspring = optimizer.crossover(primary, secondary)

    assert len(offspring.applications) == 2
    assert len(offspring.instances) == 2
    assert any(primary_instance_a.instance_id == offspring.instances.instance_id)
    assert any(primary_instance_b.instance_id == offspring.instances.instance_id)


def test_crossover_operator_reinserts_free_applications_into_existing_instance():
    """
    This test evaluates a scenario, where the crossover leads to multiple applications that are not assigned to any instance. These free applications should
    then be reinserted into the portfolio with an insertion heuristic.
    """
    # secondary a
    application_a = Application(application_id="a1",
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # secondary a
    application_b = Application(application_id="a2",
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # This application will end up as free -> should be reinserted in primary c
    application_c = Application(application_id="a3",
                                starting_time=pd.Timestamp("2021-08-05T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # primary c
    application_d = Application(application_id="a4",
                                starting_time=pd.Timestamp("2021-08-05T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # Removed
    primary_instance_a = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=5,
                                                             market_space="reserved",
                                                             price=200,
                                                             availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    # Removed
    primary_instance_b = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=5,
                                                             market_space="reserved",
                                                             price=200,
                                                             availability_start=pd.Timestamp("2021-08-05T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    # Selected
    primary_instance_c = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=5,
                                                             market_space="reserved",
                                                             price=10,
                                                             availability_start=pd.Timestamp("2021-08-05T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    primary = Portfolio(time_unit=(1, "H"))
    primary.add_instance(primary_instance_a)
    primary.add_instance(primary_instance_b)
    primary.add_instance(primary_instance_c)
    primary.add_application(primary_instance_a.instance_id, application_a)
    primary.add_application(primary_instance_b.instance_id, application_b)
    primary.add_application(primary_instance_b.instance_id, application_c)
    primary.add_application(primary_instance_c.instance_id, application_d)

    # Selected -> Better cost/utilization rate than primary a and b
    secondary_instance_a = Instance(instance_id=str(uuid.uuid4()),
                                    instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                               capacity=5,
                                                               market_space="reserved",
                                                               price=100,
                                                               availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                               availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    # Removed -> Worse cost/utilization rate than primary c
    secondary_instance_b = Instance(instance_id=str(uuid.uuid4()),
                                    instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                               capacity=10,
                                                               market_space="reserved",
                                                               price=500,
                                                               availability_start=pd.Timestamp("2021-08-05T00:00:00Z"),
                                                               availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    secondary = Portfolio(time_unit=(1, "H"))
    secondary.add_instance(secondary_instance_a)
    secondary.add_instance(secondary_instance_b)
    secondary.add_application(secondary_instance_a.instance_id, application_a)
    secondary.add_application(secondary_instance_a.instance_id, application_b)
    secondary.add_application(secondary_instance_b.instance_id, application_c)
    secondary.add_application(secondary_instance_b.instance_id, application_d)

    # This instance type serves as dummy
    instance_type = InstanceType(instance_type_id="dummy",
                                 market_space="reserved",
                                 capacity=3,
                                 price=180,
                                 availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                 availability_end=pd.Timestamp("2021-08-31T00:00:00Z"))

    optimizer = Georg([application_a, application_b], [instance_type], (1, "H"))
    offspring = optimizer.crossover(primary, secondary)

    assert len(offspring.applications) == 4
    assert len(offspring.instances) == 2
    assert any(primary_instance_c.instance_id == offspring.instances.instance_id)
    assert any(secondary_instance_a.instance_id == offspring.instances.instance_id)

    applications = offspring.get_applications(instance_ids=[primary_instance_c.instance_id])
    assert any(application_c.application_id == applications.application_id)
    assert any(application_d.application_id == applications.application_id)


def test_crossover_operator_reinserts_free_applications_into_new_instance():
    """
    This test evaluates a scenario, where the crossover leads to multiple applications that are not assigned to any instance. These free applications should
    then be reinserted into the portfolio with an insertion heuristic.
    """
    # secondary a
    application_a = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # secondary a
    application_b = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # This application will end up as free -> a new instance should be created, to fit this application
    application_c = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-05T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # primary c
    application_d = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-05T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # Removed
    primary_instance_a = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=5,
                                                             market_space="reserved",
                                                             price=200,
                                                             availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    # Removed
    primary_instance_b = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=5,
                                                             market_space="reserved",
                                                             price=200,
                                                             availability_start=pd.Timestamp("2021-08-05T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    # Selected
    primary_instance_c = Instance(instance_id=str(uuid.uuid4()),
                                  instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                             capacity=2.5,
                                                             market_space="reserved",
                                                             price=10,
                                                             availability_start=pd.Timestamp("2021-08-05T00:00:00Z"),
                                                             availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    primary = Portfolio(time_unit=(1, "H"))
    primary.add_instance(primary_instance_a)
    primary.add_instance(primary_instance_b)
    primary.add_instance(primary_instance_c)
    primary.add_application(primary_instance_a.instance_id, application_a)
    primary.add_application(primary_instance_b.instance_id, application_b)
    primary.add_application(primary_instance_b.instance_id, application_c)
    primary.add_application(primary_instance_c.instance_id, application_d)

    # Selected -> Better cost/utilization rate than primary a and b
    secondary_instance_a = Instance(instance_id=str(uuid.uuid4()),
                                    instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                               capacity=5,
                                                               market_space="reserved",
                                                               price=100,
                                                               availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                               availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    # Removed -> Worse cost/utilization rate than primary c
    secondary_instance_b = Instance(instance_id=str(uuid.uuid4()),
                                    instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                               capacity=10,
                                                               market_space="reserved",
                                                               price=500,
                                                               availability_start=pd.Timestamp("2021-08-05T00:00:00Z"),
                                                               availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    secondary = Portfolio(time_unit=(1, "H"))
    secondary.add_instance(secondary_instance_a)
    secondary.add_instance(secondary_instance_b)
    secondary.add_application(secondary_instance_a.instance_id, application_a)
    secondary.add_application(secondary_instance_a.instance_id, application_b)
    secondary.add_application(secondary_instance_b.instance_id, application_c)
    secondary.add_application(secondary_instance_b.instance_id, application_d)

    # This instance type should be selected for the reinsertion of application c
    instance_type = InstanceType(instance_type_id="app_c_host",
                                 market_space="reserved",
                                 capacity=3,
                                 price=180,
                                 availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                 availability_end=pd.Timestamp("2021-08-31T00:00:00Z"))

    optimizer = Georg([application_a, application_b], [instance_type], (1, "H"))
    offspring = optimizer.crossover(primary, secondary)

    assert len(offspring.applications) == 4
    assert len(offspring.instances) == 3
    assert any(primary_instance_c.instance_id == offspring.instances.instance_id)
    assert any(secondary_instance_a.instance_id == offspring.instances.instance_id)


def test_mutation_operator_reinserts_freed_applications_with_insertion_heuristic():
    """
    This test ensures that the applications freed by randomly removing an instance are reinserted with the insertion heuristic,
    if they do not dominate anything.
    """

    application = Application(application_id=str(uuid.uuid4()),
                              starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                              finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                              res_demand=2,
                              res_demand_deviation=0.1,
                              preemptible=False)

    instance = Instance(instance_id=str(uuid.uuid4()),
                        instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                   capacity=5,
                                                   market_space="reserved",
                                                   price=100,
                                                   availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                                   availability_end=pd.Timestamp("2021-08-07T00:00:00Z")))

    primary = Portfolio(time_unit=(1, "H"))
    primary.add_instance(instance)
    primary.add_application(instance.instance_id, application)

    instance_type = InstanceType(instance_type_id=str(uuid.uuid4()),
                                 market_space="reserved",
                                 capacity=3,
                                 price=180,
                                 availability_start=pd.Timestamp("2021-08-01T00:00:00Z"),
                                 availability_end=pd.Timestamp("2021-08-31T00:00:00Z"))

    optimizer = Georg([application], [instance_type], (1, "H"))
    mutated_chromosome = optimizer.mutate(primary, 1)

    assert len(mutated_chromosome.applications) == 1
    assert len(mutated_chromosome.instances) == 1
    # The original instance must have been deleted and replaced by a new one
    assert not all(instance.instance_id == mutated_chromosome.instances.instance_id)
    assert any(instance_type.instance_type_id == mutated_chromosome.instances.instance_type_id)


def test_mutation_operator_with_candidate_instance_available():
    """
    This test evaluates the mutation operator, where candidate instances are available. That is, two instances cover the same period. Regardless, which instance
    is eliminated the other one should be checked for the domination criterion.
    """

    # Application 1/2 dominate application 3/4
    application_1 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-09-07T00:00:00Z"),
                                res_demand=5,
                                res_demand_deviation=0.1,
                                preemptible=False)

    application_2 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-02T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-09-08T00:00:00Z"),
                                res_demand=5,
                                res_demand_deviation=0.1,
                                preemptible=False)

    application_3 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    application_4 = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    instance_a = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=15,
                                                     market_space="reserved",
                                                     price=100,
                                                     availability_start=pd.Timestamp("2021-06-01T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-10-07T00:00:00Z")))

    instance_b = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=8,
                                                     market_space="reserved",
                                                     price=100,
                                                     availability_start=pd.Timestamp("2021-06-01T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-09-10T00:00:00Z")))

    primary = Portfolio(time_unit=(1, "H"))
    primary.add_instance(instance=instance_a)
    primary.add_application(instance_id=instance_a.instance_id, application=application_1)
    primary.add_application(instance_id=instance_a.instance_id, application=application_2)

    primary.add_instance(instance=instance_b)
    primary.add_application(instance_id=instance_b.instance_id, application=application_3)
    primary.add_application(instance_id=instance_b.instance_id, application=application_4)

    instance_type = InstanceType(instance_type_id=str(uuid.uuid4()),
                                 market_space="reserved",
                                 capacity=8,
                                 price=180,
                                 availability_start=pd.Timestamp("2021-06-01T00:00:00Z"),
                                 availability_end=pd.Timestamp("2021-10-31T00:00:00Z"))

    optimizer = Georg(applications=[application_1, application_2, application_3, application_4],
                      instance_types=[instance_type],
                      time_unit=(1, "H"))
    mutated_chromosome = optimizer.mutate(primary, 1)

    # Either instance a or b should still be present
    assert any(instance_b.instance_id == mutated_chromosome.instances.instance_id) or any(instance_a.instance_id == mutated_chromosome.instances.instance_id)

    # Instance b has been eliminated
    if not any(instance_b.instance_id == mutated_chromosome.instances.instance_id):
        # Apps 3/4 have been freed, but do not dominate 1/2. The insertion heuristic should have inserted them in instance a
        assert len(mutated_chromosome.instances) == 1
        assert any(instance_a.instance_id == mutated_chromosome.instances.instance_id)

    # Instance a has been eliminated
    else:
        assert len(mutated_chromosome.instances) == 2
        assert len(mutated_chromosome.applications) == 4
        assert any(instance_b.instance_id == mutated_chromosome.instances.instance_id)

        # App 1 should end up in instance b -> app 2 does not fit instance b together with app 1 -> new instance
        # App 3 does fit into instance b (or the newly allocated one)
        # App 4 also either fits into the instance b (or the newly allocated one)
        applications = mutated_chromosome.get_applications(instance_ids=[instance_b.instance_id])
        assert len(applications) == 2
        assert any(application_1.application_id == applications.application_id)


def test_mutation_operator_where_preemptible_application_dominates(mocker):
    """
    This test ensures that the mutation operator correctly swaps applications with a subset of timeslots of a preemptible application.
    """

    application_1 = Application(
        application_id="a1",
        res_demand=5,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        preemptible=True
    )

    application_2 = Application(
        application_id="a2",
        res_demand=1,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T06:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T10:00:00Z"),
        preemptible=False
    )

    application_3 = Application(
        application_id="a3",
        res_demand=1,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T03:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T09:00:00Z"),
        preemptible=False
    )

    type_1 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=10)

    instance_1 = Instance(
        instance_id="i1",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    instance_2 = Instance(
        instance_id="i2",
        beginning_time=pd.Timestamp("2021-07-01T12:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_1
    )

    instance_3 = Instance(
        instance_id="i3",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    individual = Portfolio()
    individual.add_instance(instance_1)
    individual.add_instance(instance_2)
    individual.add_instance(instance_3)
    individual.add_application(instance_1.instance_id, application_1, pd.Timestamp("2021-07-01T00:00:00Z"), pd.Timestamp("2021-07-01T11:00:00Z"))
    individual.add_application(instance_2.instance_id, application_1, pd.Timestamp("2021-07-01T12:00:00Z"), pd.Timestamp("2021-07-01T23:00:00Z"))
    individual.add_application(instance_3.instance_id, application_2, pd.Timestamp("2021-07-01T06:00:00Z"), pd.Timestamp("2021-07-01T10:00:00Z"))
    individual.add_application(instance_3.instance_id, application_3, pd.Timestamp("2021-07-01T03:00:00Z"), pd.Timestamp("2021-07-01T09:00:00Z"))

    status = individual.check_application_assignment_validity()
    assert status["number_of_applications"] == 3
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    georg = Georg()
    georg.load_applications(applications=[application_1, application_2, application_3])
    georg.load_instance_types(instance_types=[type_1])

    mocker.patch.object(individual.instances, "sample", return_value=pd.DataFrame(index=["i1"]))
    mutated = georg.mutate(individual, number_of_mutations=1)

    status = mutated.check_application_assignment_validity()
    assert status["number_of_applications"] == 3
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    assert len(mutated.instances) == 2
    assert len(mutated.applications) == 3

    i1_applications = mutated.get_applications(instance_ids=["i3"])
    assert all([idx in i1_applications.index for idx in ["a1", "a2", "a3"]])

    i2_applications = mutated.get_applications(instance_ids=["i2"])
    assert len(i2_applications) == 1
    assert all([idx in i1_applications.index for idx in ["a1"]])


def test_mutation_operator_where_preemptible_application_is_dominated(mocker):
    """
    This test ensures that the mutation operator correctly swaps a subset of preemptible timeslots with a dominating non-preemptible application.
    The dominated preemptible app should be reinserted into a newly create instance.
    """

    application_1 = Application(
        application_id="a1",
        res_demand=5,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        preemptible=True
    )

    # Dominates a1
    application_2 = Application(
        application_id="a2",
        res_demand=9,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        preemptible=False
    )

    type_1 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=10)

    instance_1 = Instance(
        instance_id="i1",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    instance_2 = Instance(
        instance_id="i2",
        beginning_time=pd.Timestamp("2021-07-01T12:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_1
    )

    instance_3 = Instance(
        instance_id="i3",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    individual = Portfolio()
    individual.add_instance(instance_1)
    individual.add_instance(instance_2)
    individual.add_instance(instance_3)
    individual.add_application(instance_1.instance_id, application_1, pd.Timestamp("2021-07-01T00:00:00Z"), pd.Timestamp("2021-07-01T11:00:00Z"))
    individual.add_application(instance_2.instance_id, application_1, pd.Timestamp("2021-07-01T12:00:00Z"), pd.Timestamp("2021-07-01T23:00:00Z"))
    individual.add_application(instance_3.instance_id, application_2, pd.Timestamp("2021-07-01T00:00:00Z"), pd.Timestamp("2021-07-01T11:00:00Z"))

    status = individual.check_application_assignment_validity()
    assert status["number_of_applications"] == 2
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    georg = Georg()
    georg.load_applications(applications=[application_1, application_2])
    georg.load_instance_types(instance_types=[type_1])

    # Eliminate host of a2
    mocker.patch.object(individual.instances, "sample", return_value=pd.DataFrame(index=["i3"]))
    mutated = georg.mutate(individual, number_of_mutations=1)

    status = mutated.check_application_assignment_validity()
    assert status["number_of_applications"] == 2
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    assert len(mutated.instances) == 3
    assert len(mutated.applications) == 2

    i1_applications = mutated.get_applications(instance_ids=["i1"])
    assert all([idx in i1_applications.index for idx in ["a2"]])

    i2_applications = mutated.get_applications(instance_ids=["i2"])
    assert len(i2_applications) == 1
    assert all([idx in i2_applications.index for idx in ["a1"]])


def test_crossover_does_not_produce_portfolio_with_exceeding_utilization_rate():
    """
    This test ensures that the crossover operator produces a portfolio, where the utilization rate is in a valid range (i.e. < 1)
    """

    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=3,
                    res_demand_deviation=0.5,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=4,
                    res_demand_deviation=0.7,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-06T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-20T00:00:00Z"),
                    res_demand=1.2,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-25T00:00:00Z"),
                    res_demand=8,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-30T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.2,
                    preemptible=True)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=15,
                     price=200 * 720,  # reserved instances have a total price
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=5,
                     price=300),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=2.5,
                     price=250)
    ]

    primary_instances = [
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[0]),
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[0]),
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[1],
                 beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-14T00:00:00Z")),
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[2],
                 beginning_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-30T00:00:00Z"))
    ]

    secondary_instances = [
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[0]),
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[1],
                 beginning_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-14T00:00:00Z")),
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[1],
                 beginning_time=pd.Timestamp("2021-07-06T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-20T00:00:00Z")),
        Instance(instance_id=str(uuid.uuid4()),
                 instance_type=instance_types[2],
                 beginning_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                 ending_time=pd.Timestamp("2021-07-30T00:00:00Z"))
    ]

    primary = Portfolio()
    secondary = Portfolio()

    for instance in primary_instances:
        primary.add_instance(instance=instance)

    primary.add_application(instance_id=primary_instances[0].instance_id, application=applications[3])
    primary.add_application(instance_id=primary_instances[0].instance_id, application=applications[1])
    primary.add_application(instance_id=primary_instances[1].instance_id, application=applications[2])
    primary.add_application(instance_id=primary_instances[2].instance_id, application=applications[0])
    primary.add_application(instance_id=primary_instances[3].instance_id, application=applications[4])

    for instance in secondary_instances:
        secondary.add_instance(instance=instance)

    secondary.add_application(instance_id=secondary_instances[0].instance_id, application=applications[0])
    secondary.add_application(instance_id=secondary_instances[0].instance_id, application=applications[3])
    secondary.add_application(instance_id=secondary_instances[1].instance_id, application=applications[1])
    secondary.add_application(instance_id=secondary_instances[2].instance_id, application=applications[2])
    secondary.add_application(instance_id=secondary_instances[3].instance_id, application=applications[4])

    primary_stats_1 = primary.get_stats()
    secondary_stats_1 = secondary.get_stats()

    optimizer = Georg(applications=applications, instance_types=instance_types)
    offspring = optimizer.crossover(primary=primary, secondary=secondary)

    primary_stats_2 = primary.get_stats()
    secondary_stats_2 = secondary.get_stats()
    offspring_stats = offspring.get_stats()

    assert primary_stats_1["mean_utilization_overall"] == primary_stats_2["mean_utilization_overall"]
    assert secondary_stats_1["mean_utilization_overall"] == secondary_stats_2["mean_utilization_overall"]
    assert 0 < offspring_stats["mean_utilization_overall"] <= 1


def test_domination_criterion():
    """
    This test focuses on the domination criterion.
    """

    # Dominates b and c in terms of time and capacity
    application_a = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-09-07T00:00:00Z"),
                                res_demand=8,
                                res_demand_deviation=0.1,
                                preemptible=False)

    application_b = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    application_c = Application(application_id=str(uuid.uuid4()),
                                starting_time=pd.Timestamp("2021-08-01T00:00:00Z"),
                                finishing_time=pd.Timestamp("2021-08-07T00:00:00Z"),
                                res_demand=2,
                                res_demand_deviation=0.1,
                                preemptible=False)

    # Provides enough capacity for a and also covers the entire time span
    instance_a = Instance(instance_id=str(uuid.uuid4()),
                          instance_type=InstanceType(instance_type_id=str(uuid.uuid4()),
                                                     capacity=10,
                                                     market_space="reserved",
                                                     price=100,
                                                     availability_start=pd.Timestamp("2021-06-01T00:00:00Z"),
                                                     availability_end=pd.Timestamp("2021-10-07T00:00:00Z")))

    portfolio = Portfolio(time_unit=(1, "H"))
    portfolio.add_instance(instance=instance_a)
    portfolio.add_application(instance_id=instance_a.instance_id, application=application_a)
    portfolio.add_application(instance_id=instance_a.instance_id, application=application_b)
    portfolio.add_application(instance_id=instance_a.instance_id, application=application_c)

    assert portfolio.dominates(instance_id=instance_a.instance_id,
                               dominating_id=application_a.application_id,
                               dominated_ids=[application_b.application_id, application_c.application_id],
                               from_time=application_a.starting_time,
                               to_time=application_a.finishing_time) is True

    assert portfolio.dominates(instance_id=instance_a.instance_id,
                               dominating_id=application_b.application_id,
                               dominated_ids=[application_a.application_id, application_c.application_id],
                               from_time=application_b.starting_time,
                               to_time=application_b.finishing_time) is False

    # App a does not dominate app b alone, because the instance would not provide enough capacity since app c still needs to be considered
    assert portfolio.dominates(instance_id=instance_a.instance_id,
                               dominating_id=application_a.application_id,
                               dominated_ids=[application_b.application_id],
                               from_time=application_a.starting_time,
                               to_time=application_a.finishing_time) is False


@pytest.mark.skip(reason="Test execution takes long and should not be included in remote test runner.")
def test_entire_evolution_process():
    """
    This test ensures that the entire evolution process is carried out without any failures. The end result should be a list of portfolios, sorted by fitness.
    """
    applications = [
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=3,
                    res_demand_deviation=0.5,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-14T00:00:00Z"),
                    res_demand=4,
                    res_demand_deviation=0.7,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-06T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-20T00:00:00Z"),
                    res_demand=1.2,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-25T00:00:00Z"),
                    res_demand=8,
                    res_demand_deviation=0.1,
                    preemptible=False),
        Application(application_id=str(uuid.uuid4()),
                    starting_time=pd.Timestamp("2021-07-07T00:00:00Z"),
                    finishing_time=pd.Timestamp("2021-07-30T00:00:00Z"),
                    res_demand=1,
                    res_demand_deviation=0.2,
                    preemptible=True)
    ]

    instance_types = [
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=15,
                     price=200 * 720,  # reserved instances have a total price
                     availability_start=pd.Timestamp("2021-07-01T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="reserved",
                     capacity=6,
                     price=100 * 504,
                     availability_start=pd.Timestamp("2021-07-10T00:00:00Z"),
                     availability_end=pd.Timestamp("2021-07-31T00:00:00Z")),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=5,
                     price=300),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="on_demand",
                     capacity=2.5,
                     price=250),
        InstanceType(instance_type_id=str(uuid.uuid4()),
                     market_space="spot",
                     capacity=5,
                     price=60)
    ]

    optimizer = Georg(applications=applications, instance_types=instance_types, time_unit=(1, "H"))

    portfolios, generations = optimizer.optimize(population_size=10,
                                                 number_of_generations=15,
                                                 max_generations_without_improvements=5,
                                                 convergence_threshold=2,
                                                 parent_selection_strategy="tournament",
                                                 number_of_offspring=2,
                                                 mutation_rate=0.3,
                                                 number_of_mutations=3,
                                                 survivor_selection_strategy="fittest_overall")

    assert len(portfolios) == 10

    status = portfolios[0].check_application_assignment_validity()

    assert status["number_of_applications"] == 5
    for key, value in status.items():
        if key != "number_of_applications":
            assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
            assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"


@pytest.mark.skip(reason="Test execution takes long and should not be included in remote test runner.")
def test_crossover_does_not_lose_apps():
    """
    This test ensures that the crossover operator produces an offspring with the same number of apps as the parents.
    """
    with open(Path(__file__).parent.joinpath("test_data/couple.bin"), "rb") as f:
        couple = pickle.load(f)

    optimizer = Georg()

    child = optimizer.crossover(primary=couple[0], secondary=couple[1])

    status = child.check_application_assignment_validity()

    assert status["number_of_applications"] == 100
    for key, value in status.items():
        if key != "number_of_applications":
            assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
            assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"


@pytest.mark.skip(reason="Test requires binary data.")
def test_crossover_does_not_produce_assignment_exceeding_capacity():
    """
    This test ensures that the crossover operator produces an offspring where the exceedence probability is within the limit.
    """
    with open(Path(__file__).parent.joinpath("invalid_probs/couple.bin"), "rb") as f:
        couple = pickle.load(f)

    optimizer = Georg()
    optimizer.load_applications("../evaluation/data/applications_100_week.csv")
    optimizer.load_instance_types("../evaluation/data/instance_types_week.csv")

    child = optimizer.crossover(primary=couple[0], secondary=couple[1])

    status = child.check_application_assignment_validity()

    assert status["number_of_applications"] == 100
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"


def test_crossover_where_spot_app_is_split_over_two_instances():
    """
    This test ensures that spot applications, that are split over two instances, are inherited correctly. It assumes that the secondary parent has an allocation
    with one instance, that is more expensive than the dual assignment of the primary parent.
    """

    application = Application(
        application_id="a1",
        res_demand=1,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        preemptible=True
    )

    type_1 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=10)

    type_2 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=100)  # worse price -> the secondary parent should be ranked worse and the primary pattern should be inherited

    instance_1 = Instance(
        instance_id="i1",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    instance_2 = Instance(
        instance_id="i2",
        beginning_time=pd.Timestamp("2021-07-01T12:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_1
    )

    instance_3 = Instance(
        instance_id="i3",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_2
    )

    primary = Portfolio()
    primary.add_instance(instance_1)
    primary.add_instance(instance_2)
    primary.add_application(instance_1.instance_id, application, from_time=pd.Timestamp("2021-07-01T00:00:00Z"), to_time=pd.Timestamp("2021-07-01T11:00:00Z"))
    primary.add_application(instance_2.instance_id, application, from_time=pd.Timestamp("2021-07-01T12:00:00Z"), to_time=pd.Timestamp("2021-07-01T23:00:00Z"))

    secondary = Portfolio()
    secondary.add_instance(instance_3)
    secondary.add_application(instance_3.instance_id, application, from_time=pd.Timestamp("2021-07-01T00:00:00Z"), to_time=pd.Timestamp("2021-07-01T23:00:00Z"))

    primary_status = primary.check_application_assignment_validity()
    assert primary_status["number_of_applications"] == 1
    assert primary_status["within_qos"] == True
    for key, value in primary_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    secondary_status = secondary.check_application_assignment_validity()
    assert secondary_status["number_of_applications"] == 1
    assert secondary_status["within_qos"] == True
    for key, value in secondary_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    georg = Georg()
    georg.load_applications(applications=[application])
    georg.load_instance_types(instance_types=[type_1, type_2])

    offspring = georg.crossover(primary, secondary)

    offspring_status = secondary.check_application_assignment_validity()
    assert offspring_status["number_of_applications"] == 1
    assert offspring_status["within_qos"] == True
    for key, value in offspring_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    assert len(offspring.instances) == 2
    assert offspring.packing_pattern.equals(primary.packing_pattern)
    assert offspring.instance_usage.equals(primary.instance_usage)


def test_crossover_where_single_instance_allocation_is_preferred_over_split_one():
    """
    This test ensures that spot applications, that are assigned to a single instance are inherited correctly. It assumes that the parent with the worse allocation
    has the application split over two instances, while the better allocation only includes one instance.
    """

    application = Application(
        application_id="a1",
        res_demand=1,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        preemptible=True
    )

    type_1 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=100)  # worse price -> the secondary parent should be ranked worse and the primary pattern should be inherited

    type_2 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=10)

    instance_1 = Instance(
        instance_id="i1",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    instance_2 = Instance(
        instance_id="i2",
        beginning_time=pd.Timestamp("2021-07-01T12:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_1
    )

    instance_3 = Instance(
        instance_id="i3",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_2
    )

    primary = Portfolio()
    primary.add_instance(instance_1)
    primary.add_instance(instance_2)
    primary.add_application(instance_1.instance_id, application, from_time=pd.Timestamp("2021-07-01T00:00:00Z"), to_time=pd.Timestamp("2021-07-01T11:00:00Z"))
    primary.add_application(instance_2.instance_id, application, from_time=pd.Timestamp("2021-07-01T12:00:00Z"), to_time=pd.Timestamp("2021-07-01T23:00:00Z"))

    secondary = Portfolio()
    secondary.add_instance(instance_3)
    secondary.add_application(instance_3.instance_id, application, from_time=pd.Timestamp("2021-07-01T00:00:00Z"), to_time=pd.Timestamp("2021-07-01T23:00:00Z"))

    primary_status = primary.check_application_assignment_validity()
    assert primary_status["number_of_applications"] == 1
    assert primary_status["within_qos"] == True
    for key, value in primary_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    secondary_status = secondary.check_application_assignment_validity()
    assert secondary_status["number_of_applications"] == 1
    assert secondary_status["within_qos"] == True
    for key, value in secondary_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    georg = Georg()
    georg.load_applications(applications=[application])
    georg.load_instance_types(instance_types=[type_1, type_2])

    offspring = georg.crossover(primary, secondary)

    offspring_status = offspring.check_application_assignment_validity()
    assert offspring_status["number_of_applications"] == 1
    assert offspring_status["within_qos"] == True
    for key, value in offspring_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    assert len(offspring.instances) == 1
    assert offspring.packing_pattern.equals(secondary.packing_pattern)
    assert offspring.instance_usage.equals(secondary.instance_usage)


def test_crossover_where_spot_application_is_allocated_for_a_subset_of_time_slots_by_insertion_heuristic():
    """
    This test ensures that spot applications, where allocations are only partly inherited, are correctly assigned by the insertion heuristic.
    """

    application_1 = Application(
        application_id="a1",
        res_demand=1,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        preemptible=True
    )

    application_2 = Application(
        application_id="a2",
        res_demand=1,
        res_demand_deviation=0.1,
        starting_time=pd.Timestamp("2021-07-01T12:00:00Z"),
        finishing_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        preemptible=True
    )

    type_1 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=10)

    type_2 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=10,
                          price=100)

    type_3 = InstanceType(instance_type_id=str(uuid.uuid4()),
                          market_space="on_demand",
                          capacity=3,
                          price=0.5)  # Cheaper than type 1

    instance_1 = Instance(
        instance_id="i1",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T11:00:00Z"),
        instance_type=type_1
    )

    instance_2 = Instance(
        instance_id="i2",
        beginning_time=pd.Timestamp("2021-07-01T12:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_1
    )

    instance_3 = Instance(
        instance_id="i3",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_2
    )

    instance_4 = Instance(
        instance_id="i4",
        beginning_time=pd.Timestamp("2021-07-01T00:00:00Z"),
        ending_time=pd.Timestamp("2021-07-01T23:00:00Z"),
        instance_type=type_3
    )

    primary = Portfolio()
    primary.add_instance(instance_1)
    primary.add_instance(instance_2)
    primary.add_application(instance_1.instance_id, application_1, pd.Timestamp("2021-07-01T00:00:00Z"), pd.Timestamp("2021-07-01T11:00:00Z"))
    primary.add_application(instance_2.instance_id, application_1, pd.Timestamp("2021-07-01T12:00:00Z"), pd.Timestamp("2021-07-01T23:00:00Z"))
    primary.add_application(instance_2.instance_id, application_2, pd.Timestamp("2021-07-01T12:00:00Z"), pd.Timestamp("2021-07-01T23:00:00Z"))

    secondary = Portfolio()
    secondary.add_instance(instance_3)
    secondary.add_instance(instance_4)
    secondary.add_application(instance_3.instance_id, application_1, pd.Timestamp("2021-07-01T00:00:00Z"), pd.Timestamp("2021-07-01T23:00:00Z"))
    secondary.add_application(instance_4.instance_id, application_2,  pd.Timestamp("2021-07-01T12:00:00Z"), pd.Timestamp("2021-07-01T23:00:00Z"))

    primary_status = primary.check_application_assignment_validity()
    assert primary_status["number_of_applications"] == 2
    assert primary_status["within_qos"] == True
    for key, value in primary_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    secondary_status = secondary.check_application_assignment_validity()
    assert secondary_status["number_of_applications"] == 2
    assert secondary_status["within_qos"] == True
    for key, value in secondary_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    georg = Georg()
    georg.load_applications(applications=[application_1, application_2])
    georg.load_instance_types(instance_types=[type_1, type_2])

    offspring = georg.crossover(primary, secondary)

    offspring_status = offspring.check_application_assignment_validity()
    assert offspring_status["number_of_applications"] == 2
    assert offspring_status["within_qos"] == True
    for key, value in offspring_status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"

    assert len(offspring.instances) == 2
    assert len(offspring.applications) == 2

    i1_packing_pattern = offspring.get_packing_pattern(instance_ids=["i1"])
    assert not i1_packing_pattern.empty and all(i1_packing_pattern.index.isin(["a1"], level=2))

    i4_packing_pattern = offspring.get_packing_pattern(instance_ids=["i4"])
    assert not i4_packing_pattern.empty and all(i4_packing_pattern.index.isin(["a1", "a2"], level=2))


@pytest.mark.skip(reason="Test requires binary data.")
def test_crossover_does_not_produce_invalid_instance_assignments():
    """
    This test ensures that the crossover operator produces an offspring where the exceedence probability is within the limit.
    """
    with open(Path(__file__).parent.joinpath("invalid_instance_assignments/couple.bin"), "rb") as f:
        couple = pickle.load(f)

    optimizer = Georg()
    optimizer.load_applications("../evaluation/data/applications_20_week.csv")
    optimizer.load_instance_types("../evaluation/data/instance_types_week.csv")

    child = optimizer.crossover(primary=couple[0], secondary=couple[1])

    status = child.check_application_assignment_validity()

    assert status["number_of_applications"] == 20
    assert status["within_qos"] == True
    for key, value in status["applications"].items():
        assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
        assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"
        assert value["invalid_assignments"] == False, f"Invalid application assignments for {key}"
    for key, value in status["instances"].items():
        assert value["invalid_assignments"] == False, f"Invalid instance assignments for {key}"
        assert value["invalid_usage"] == False, f"Invalid instance usage for {key}"
