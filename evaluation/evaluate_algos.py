import timeit
from pathlib import Path

from typing import Dict, Tuple

import pandas as pd

from cloud_portfolio_manager.optimizers import Erich, Georg

timeit.template = """
def inner(_it, _timer{init}):
    {setup}
    _t0 = _timer()
    for _i in _it:
        retval = {stmt}
    _t1 = _timer()
    return _t1 - _t0, retval 
"""


def evaluate_test_cases(method: str,
                        test_cases: Dict[str, Tuple[str, str]],
                        label: str,
                        opts: Dict[any, any] = None,
                        number_of_evaluations: int = 10,
                        check_validity=False):
    opts = opts or {}

    instances_index = pd.MultiIndex(levels=[[], [], [], [], []],
                                    codes=[[], [], [], [], []],
                                    names=["method", "test_case", "iteration", "rank", "instance_id"])

    instances_results = pd.DataFrame(index=instances_index,
                                     columns=["instance_id", "beginning_time", "ending_time", "instance_type_id", "market_space", "capacity",
                                              "price", "availability_start", "availability_end", "utilization_total", "utilization_rate",
                                              "utilization_efficiency"])

    columns = ["run_time", "number_of_hosts_overall", "number_of_hosts_reserved", "number_of_hosts_on_demand", "number_of_hosts_spot",
               "mean_utilization_overall", "mean_utilization_reserved", "mean_utilization_on_demand", "mean_utilization_spot",
               "weighted_utilization_overall", "weighted_utilization_reserved", "weighted_utilization_on_demand",
               "weighted_utilization_spot", "cost_overall"]

    result_index = pd.MultiIndex(levels=[[], [], [], []],
                                 codes=[[], [], [], []],
                                 names=["method", "test_case", "iteration", "rank"])

    results = pd.DataFrame(index=result_index,
                           columns=columns)

    generation_index = pd.MultiIndex(levels=[[], [], [], []],
                                     codes=[[], [], [], []],
                                     names=["test_case", "iteration", "generation", "rank"])

    generation_results = pd.DataFrame(index=generation_index,
                                      columns=columns[1:])

    for test_case, files in test_cases.items():
        print(f"Performing evaluation for {method}: {files[0]}, {files[1]}")

        stmt = f"optimizer.optimize(**opts)"
        optimizer = Erich() if method == "erich" else Georg()
        optimizer.load_applications(path=files[0])
        optimizer.load_instance_types(path=files[1])

        # Don't use timeit's iteration function, since we want individual results and also do not want to distort the measurements with additional operations
        for iteration in range(number_of_evaluations):
            run_time, optimization = timeit.timeit(stmt=stmt, number=1, globals=locals())

            # Process the end results
            for rank, result in enumerate(optimization[0] if isinstance(optimization, tuple) else [optimization]):
                data = {"run_time": run_time, **result.get_stats()}
                results.loc[(method, test_case, iteration, rank), :] = data

                # Collect instance data
                tmp_df = result.instances.copy()
                tmp_df = tmp_df.reset_index(drop=True)
                tmp_df = tmp_df.assign(method=method, test_case=test_case, iteration=iteration, rank=rank)
                tmp_df = tmp_df.set_index(["method", "test_case", "iteration", "rank", "instance_id"])
                instances_results = pd.concat([instances_results, tmp_df])

                # Save the instances/applications/packing pattern and instance usage
                path = Path(f"./results/{method}/{test_case}/{iteration}/{rank}")
                if not path.is_dir():
                    path.mkdir(parents=True)
                result.save(path)

            # Process the generational data from GEORG
            if isinstance(optimization, tuple):
                for generation, population in enumerate(optimization[1]):
                    for rank, individual in enumerate(population):
                        generation_results.loc[(test_case, iteration, generation, rank)] = individual.get_stats()

                portfolios_to_evaluate = optimization[0]
            else:
                portfolios_to_evaluate = [optimization]

            if check_validity:
                for idx, portfolio in enumerate(portfolios_to_evaluate):

                    number_of_applications = int(files[0].split("_")[1])
                    status = portfolio.check_application_assignment_validity()

                    try:
                        assert status["number_of_applications"] == number_of_applications, f"Invalid number of applications"
                        assert status["within_qos"] == True, "Invalid QoS"
                        for key, value in status["applications"].items():
                            assert value["intended_assignments"] == value["actual_assignments"], f"Invalid number of assignments for {key}"
                            assert value["duplicates"] is None, f"Found duplicates for {key}: {value['duplicates']}"
                            assert value["invalid_assignments"] == False, f"Invalid application assignments for {key}"
                        for key, value in status["instances"].items():
                            assert value["invalid_assignments"] == False, f"Invalid instance assignments for {key}"
                            assert value["invalid_usage"] == False, f"Invalid instance usage for {key}"
                    except AssertionError as ex:
                        df_apps = pd.read_csv(files[0])
                        df_apps.set_index("application_id", drop=False, inplace=True)
                        missing_apps = df_apps[~df_apps.index.isin(status.keys())]
                        print(missing_apps)
                        print(f"AssertionError in {method}/{test_case}/{iteration}/{idx}")
                        print(ex)

                print("Results validated.")

    results.to_csv(f"results\\{method}_{label}.csv")
    instances_results.to_csv(f"results\\{method}_instances_{label}.csv")

    if not generation_results.empty:
        generation_results.to_csv(f"results\\generations_{label}.csv")


if __name__ == "__main__":
    test_cases = {
        "case_1": ("./data/applications_20_week.csv", "./data/instance_types_week.csv"),
        "case_2": ("./data/applications_100_week.csv", "./data/instance_types_week.csv"),
        "case_3": ("./data/applications_20_month.csv", "./data/instance_types_month.csv"),
        "case_4": ("./data/applications_100_month.csv", "./data/instance_types_month.csv"),
        "case_5": ("./data/applications_20_year.csv", "./data/instance_types_year.csv"),
        "case_6": ("./data/applications_100_year.csv", "./data/instance_types_year.csv")
    }

    # Evaluate ERICH
    evaluate_test_cases(method="erich", test_cases=test_cases, number_of_evaluations=10, label="final", check_validity=True)

    # Evaluate GEORG
    options = {"population_size": 15,
               "number_of_generations": 10,
               "max_generations_without_improvements": 2,
               "convergence_threshold": 0,
               "parent_selection_strategy": "roulette_wheel",
               "number_of_offspring": 7,
               "mutation_rate": 0.1,
               "number_of_mutations": 2,
               "survivor_selection_strategy": "fittest_overall"}
    evaluate_test_cases(method="georg", test_cases=test_cases, number_of_evaluations=10, label="final", opts=options, check_validity=True)
