import random
from pathlib import Path

import pandas as pd
from matplotlib import pyplot as plt

#
# def set_box_color(bp, color):
#     plt.setp(bp['boxes'], color=color)
#     plt.setp(bp['whiskers'], color=color)
#     plt.setp(bp['caps'], color=color)
#     plt.setp(bp['medians'], color=color)


if __name__ == "__main__":

    output_path = Path(__file__).parent.joinpath("figures")
    label = "final"
    erich_data = pd.read_csv(f"./results/erich_{label}.csv", delimiter=",")
    georg_data = pd.read_csv(f"./results/georg_{label}.CSV", delimiter=",")

    # Box Plots for ERICH performance.
    # Each test case should have its own box plot
    erich_run_time_axs = erich_data.boxplot(column="run_time", by="test_case")
    erich_run_time_axs.set_xlabel("Test Case")
    erich_run_time_axs.set_ylabel("Seconds")
    plt.title("Runtime Comparison ERICH")
    plt.suptitle('')
    # erich_run_time_axs.set_ylabel("seconds")
    erich_run_time_axs.figure.savefig(output_path.joinpath("erich_run_time.png"))
    erich_run_time_axs.figure.savefig(output_path.joinpath("erich_run_time.svg"))

    # Box Plots for GEORG performance.
    georg_run_time_axs = georg_data.boxplot(column="run_time", by="test_case")
    georg_run_time_axs.set_xlabel("Test Case")
    georg_run_time_axs.set_ylabel("Seconds")
    plt.title("Runtime Comparison GEORG")
    plt.suptitle('')
    # georg_run_time_axs.set_ylabel("seconds")
    georg_run_time_axs.figure.savefig(output_path.joinpath("georg_run_time.png"))
    georg_run_time_axs.figure.savefig(output_path.joinpath("georg_run_time.svg"))

    rand_iteration = random.randint(0, min(georg_data["iteration"].max(), erich_data["iteration"].max()))
    filtered_erich_data = erich_data[erich_data["iteration"] == rand_iteration]
    filtered_georg_data = georg_data[georg_data["iteration"] == rand_iteration]
    worst_rank = filtered_georg_data["rank"].max()

    # Total number of hosts
    # Bar graph.
    # X axis are the test cases.
    # Each test cases represents a category for which 4 subgroups are shown.
    # - ERICH always has a deterministic number of hosts per test case and therefore only requires a single subcategory
    # - GEORG has a best category, a worst category and an average of the entire population (but not of all iterations).
    total_hosts_erich = filtered_erich_data[["test_case", "number_of_hosts_overall"]].rename({"number_of_hosts_overall": "erich"}, axis=1)
    total_hosts_erich = total_hosts_erich.set_index("test_case")

    total_hosts_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "number_of_hosts_overall"]].rename(
        {"number_of_hosts_overall": "best_georg"}, axis=1)
    total_hosts_georg_best = total_hosts_georg_best.set_index("test_case")

    total_hosts_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "number_of_hosts_overall"]].rename(
        {"number_of_hosts_overall": "worst_georg"}, axis=1)
    total_hosts_georg_worst = total_hosts_georg_worst.set_index("test_case")

    total_hosts_georg_avg = filtered_georg_data[["test_case", "number_of_hosts_overall"]].groupby("test_case").mean().rename(
        {"number_of_hosts_overall": "avg_georg"}, axis=1)

    total_hosts_plot_data = pd.concat([total_hosts_erich, total_hosts_georg_best, total_hosts_georg_avg, total_hosts_georg_worst], axis=1)
    total_hosts_plot_axis = total_hosts_plot_data.plot.bar()
    total_hosts_plot_axis.set_xlabel("Test Case")
    total_hosts_plot_axis.set_ylabel("Number of Instances")
    plt.title("Total Number of Instances")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    total_hosts_plot_axis.figure.savefig(output_path.joinpath("total_number_of_hosts.png"))
    total_hosts_plot_axis.figure.savefig(output_path.joinpath("total_number_of_hosts.svg"))

    # Number of hosts per type
    # Reserved
    total_hosts_reserved_erich = filtered_erich_data[["test_case", "number_of_hosts_reserved"]].rename({"number_of_hosts_reserved": "erich"}, axis=1)
    total_hosts_reserved_erich = total_hosts_reserved_erich.set_index("test_case")

    total_hosts_reserved_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "number_of_hosts_reserved"]].rename(
        {"number_of_hosts_reserved": "best_georg"}, axis=1)
    total_hosts_reserved_georg_best = total_hosts_reserved_georg_best.set_index("test_case")

    total_hosts_reserved_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "number_of_hosts_reserved"]].rename(
        {"number_of_hosts_reserved": "worst_georg"}, axis=1)
    total_hosts_reserved_georg_worst = total_hosts_reserved_georg_worst.set_index("test_case")

    total_hosts_reserved_georg_avg = filtered_georg_data[["test_case", "number_of_hosts_reserved"]].groupby("test_case").mean().rename(
        {"number_of_hosts_reserved": "avg_georg"}, axis=1)

    total_hosts_reserved_plot_data = pd.concat(
        [total_hosts_reserved_erich, total_hosts_reserved_georg_best, total_hosts_reserved_georg_avg, total_hosts_reserved_georg_worst], axis=1)
    total_hosts_reserved_plot_axis = total_hosts_reserved_plot_data.plot.bar()
    total_hosts_reserved_plot_axis.set_xlabel("Test Case")
    total_hosts_reserved_plot_axis.set_ylabel("Number of Reserved Instances")
    plt.title("Total Number of Reserved Instances")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    total_hosts_reserved_plot_axis.figure.savefig(output_path.joinpath("total_number_of_reserved_hosts.png"))
    total_hosts_reserved_plot_axis.figure.savefig(output_path.joinpath("total_number_of_reserved_hosts.svg"))

    # On-Demand
    total_hosts_on_demand_erich = filtered_erich_data[["test_case", "number_of_hosts_on_demand"]].rename({"number_of_hosts_on_demand": "erich"}, axis=1)
    total_hosts_on_demand_erich = total_hosts_on_demand_erich.set_index("test_case")

    total_hosts_on_demand_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "number_of_hosts_on_demand"]].rename(
        {"number_of_hosts_on_demand": "best_georg"}, axis=1)
    total_hosts_on_demand_georg_best = total_hosts_on_demand_georg_best.set_index("test_case")

    total_hosts_on_demand_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "number_of_hosts_on_demand"]].rename(
        {"number_of_hosts_on_demand": "worst_georg"}, axis=1)
    total_hosts_on_demand_georg_worst = total_hosts_on_demand_georg_worst.set_index("test_case")

    total_hosts_on_demand_georg_avg = filtered_georg_data[["test_case", "number_of_hosts_on_demand"]].groupby("test_case").mean().rename(
        {"number_of_hosts_on_demand": "avg_georg"}, axis=1)

    total_hosts_on_demand_plot_data = pd.concat(
        [total_hosts_on_demand_erich, total_hosts_on_demand_georg_best, total_hosts_on_demand_georg_avg, total_hosts_on_demand_georg_worst], axis=1)
    total_hosts_on_demand_plot_axis = total_hosts_on_demand_plot_data.plot.bar()
    total_hosts_on_demand_plot_axis.set_xlabel("Test Case")
    total_hosts_on_demand_plot_axis.set_ylabel("Number of On-Demand Instances")
    plt.title("Total Number of On-Demand Instances")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    total_hosts_on_demand_plot_axis.figure.savefig(output_path.joinpath("total_number_of_on_demand_hosts.png"))
    total_hosts_on_demand_plot_axis.figure.savefig(output_path.joinpath("total_number_of_on_demand_hosts.svg"))

    # Spot
    total_hosts_spot_erich = filtered_erich_data[["test_case", "number_of_hosts_spot"]].rename({"number_of_hosts_spot": "erich"}, axis=1)
    total_hosts_spot_erich = total_hosts_spot_erich.set_index("test_case")

    total_hosts_spot_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "number_of_hosts_spot"]].rename(
        {"number_of_hosts_spot": "best_georg"}, axis=1)
    total_hosts_spot_georg_best = total_hosts_spot_georg_best.set_index("test_case")

    total_hosts_spot_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "number_of_hosts_spot"]].rename(
        {"number_of_hosts_spot": "worst_georg"}, axis=1)
    total_hosts_spot_georg_worst = total_hosts_spot_georg_worst.set_index("test_case")

    total_hosts_spot_georg_avg = filtered_georg_data[["test_case", "number_of_hosts_spot"]].groupby("test_case").mean().rename(
        {"number_of_hosts_spot": "avg_georg"}, axis=1)

    total_hosts_spot_plot_data = pd.concat([total_hosts_spot_erich, total_hosts_spot_georg_best, total_hosts_spot_georg_avg, total_hosts_spot_georg_worst],
                                           axis=1)
    total_hosts_spot_plot_axis = total_hosts_spot_plot_data.plot.bar()
    total_hosts_spot_plot_axis.set_xlabel("Test Case")
    total_hosts_spot_plot_axis.set_ylabel("Number of Spot Instances")
    plt.title("Total Number of Spot Instances")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    total_hosts_spot_plot_axis.figure.savefig(output_path.joinpath("total_number_of_spot_hosts.png"))
    total_hosts_spot_plot_axis.figure.savefig(output_path.joinpath("total_number_of_spot_hosts.svg"))

    # Total utilization rate
    # Bar graph.
    # X axis are the test cases.
    # Each test cases represents a category for which 4 subgroups are shown.
    # - ERICH always has a deterministic number of hosts per test case and therefore only requires a single subcategory
    # - GEORG has a best category, a worst category and an average of the entire population (but not of all iterations).
    total_utilization_erich = filtered_erich_data[["test_case", "mean_utilization_overall"]].rename({"mean_utilization_overall": "erich"}, axis=1)
    total_utilization_erich = total_utilization_erich.set_index("test_case")

    total_utilization_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "mean_utilization_overall"]].rename(
        {"mean_utilization_overall": "best_georg"}, axis=1)
    total_utilization_georg_best = total_utilization_georg_best.set_index("test_case")

    total_utilization_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "mean_utilization_overall"]].rename(
        {"mean_utilization_overall": "worst_georg"}, axis=1)
    total_utilization_georg_worst = total_utilization_georg_worst.set_index("test_case")

    total_utilization_georg_avg = filtered_georg_data[["test_case", "mean_utilization_overall"]].groupby("test_case").mean().rename(
        {"mean_utilization_overall": "avg_georg"}, axis=1)

    total_utilization_plot_data = pd.concat([total_utilization_erich, total_utilization_georg_best, total_utilization_georg_avg, total_utilization_georg_worst],
                                            axis=1)
    total_utilization_plot_axis = total_utilization_plot_data.plot.bar()
    total_utilization_plot_axis.set_xlabel("Test Case")
    total_utilization_plot_axis.set_ylabel("Utilization Rate")
    plt.title("Total Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    total_utilization_plot_axis.figure.savefig(output_path.joinpath("total_utilization.png"))
    total_utilization_plot_axis.figure.savefig(output_path.joinpath("total_utilization.svg"))

    # Total reserved utilization
    reserved_utilization_erich = filtered_erich_data[["test_case", "mean_utilization_reserved"]].rename({"mean_utilization_reserved": "erich"}, axis=1)
    reserved_utilization_erich = reserved_utilization_erich.set_index("test_case")

    reserved_utilization_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "mean_utilization_reserved"]].rename(
        {"mean_utilization_reserved": "best_georg"}, axis=1)
    reserved_utilization_georg_best = reserved_utilization_georg_best.set_index("test_case")

    reserved_utilization_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "mean_utilization_reserved"]].rename(
        {"mean_utilization_reserved": "worst_georg"}, axis=1)
    reserved_utilization_georg_worst = reserved_utilization_georg_worst.set_index("test_case")

    reserved_utilization_georg_avg = filtered_georg_data[["test_case", "mean_utilization_reserved"]].groupby("test_case").mean().rename(
        {"mean_utilization_reserved": "avg_georg"}, axis=1)

    reserved_utilization_plot_data = pd.concat(
        [reserved_utilization_erich, reserved_utilization_georg_best, reserved_utilization_georg_avg, reserved_utilization_georg_worst], axis=1)
    reserved_utilization_plot_axis = reserved_utilization_plot_data.plot.bar()
    reserved_utilization_plot_axis.set_xlabel("Test Case")
    reserved_utilization_plot_axis.set_ylabel("Utilization Rate")
    plt.title("Reserved Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    reserved_utilization_plot_axis.figure.savefig(output_path.joinpath("reserved_utilization.png"))
    reserved_utilization_plot_axis.figure.savefig(output_path.joinpath("reserved_utilization.svg"))

    # On-demand utilization
    on_demand_utilization_erich = filtered_erich_data[["test_case", "mean_utilization_on_demand"]].rename({"mean_utilization_on_demand": "erich"}, axis=1)
    on_demand_utilization_erich = on_demand_utilization_erich.set_index("test_case")

    on_demand_utilization_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "mean_utilization_on_demand"]].rename(
        {"mean_utilization_on_demand": "best_georg"}, axis=1)
    on_demand_utilization_georg_best = on_demand_utilization_georg_best.set_index("test_case")

    on_demand_utilization_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "mean_utilization_on_demand"]].rename(
        {"mean_utilization_on_demand": "worst_georg"}, axis=1)
    on_demand_utilization_georg_worst = on_demand_utilization_georg_worst.set_index("test_case")

    on_demand_utilization_georg_avg = filtered_georg_data[["test_case", "mean_utilization_on_demand"]].groupby("test_case").mean().rename(
        {"mean_utilization_on_demand": "avg_georg"}, axis=1)

    on_demand_utilization_plot_data = pd.concat(
        [on_demand_utilization_erich, on_demand_utilization_georg_best, on_demand_utilization_georg_avg, on_demand_utilization_georg_worst], axis=1)
    on_demand_utilization_plot_axis = on_demand_utilization_plot_data.plot.bar()
    on_demand_utilization_plot_axis.set_xlabel("Test Case")
    on_demand_utilization_plot_axis.set_ylabel("Utilization Rate")
    plt.title("On-Demand Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    on_demand_utilization_plot_axis.figure.savefig(output_path.joinpath("on_demand_utilization.png"))
    on_demand_utilization_plot_axis.figure.savefig(output_path.joinpath("on_demand_utilization.svg"))

    # Spot utilization
    spot_utilization_erich = filtered_erich_data[["test_case", "mean_utilization_spot"]].rename({"mean_utilization_spot": "erich"}, axis=1)
    spot_utilization_erich = spot_utilization_erich.set_index("test_case")

    spot_utilization_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "mean_utilization_spot"]].rename(
        {"mean_utilization_spot": "best_georg"}, axis=1)
    spot_utilization_georg_best = spot_utilization_georg_best.set_index("test_case")

    spot_utilization_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "mean_utilization_spot"]].rename(
        {"mean_utilization_spot": "worst_georg"}, axis=1)
    spot_utilization_georg_worst = spot_utilization_georg_worst.set_index("test_case")

    spot_utilization_georg_avg = filtered_georg_data[["test_case", "mean_utilization_spot"]].groupby("test_case").mean().rename(
        {"mean_utilization_spot": "avg_georg"}, axis=1)

    spot_utilization_plot_data = pd.concat([spot_utilization_erich, spot_utilization_georg_best, spot_utilization_georg_avg, spot_utilization_georg_worst],
                                           axis=1)
    spot_utilization_plot_axis = spot_utilization_plot_data.plot.bar()
    spot_utilization_plot_axis.set_xlabel("Test Case")
    spot_utilization_plot_axis.set_ylabel("Utilization Rate")
    plt.title("Spot Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    spot_utilization_plot_axis.figure.savefig(output_path.joinpath("spot_utilization.png"))
    spot_utilization_plot_axis.figure.savefig(output_path.joinpath("spot_utilization.svg"))

    # Weighted total utilization rate
    # Bar graph.
    # X axis are the test cases.
    # Each test cases represents a category for which 4 subgroups are shown.
    # - ERICH always has a deterministic number of hosts per test case and therefore only requires a single subcategory
    # - GEORG has a best category, a worst category and an average of the entire population (but not of all iterations).
    weighted_utilization_erich = filtered_erich_data[["test_case", "weighted_utilization_overall"]].rename({"weighted_utilization_overall": "erich"}, axis=1)
    weighted_utilization_erich = weighted_utilization_erich.set_index("test_case")

    weighted_utilization_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "weighted_utilization_overall"]].rename(
        {"weighted_utilization_overall": "best_georg"}, axis=1)
    weighted_utilization_georg_best = weighted_utilization_georg_best.set_index("test_case")

    weighted_utilization_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "weighted_utilization_overall"]].rename(
        {"weighted_utilization_overall": "worst_georg"}, axis=1)
    weighted_utilization_georg_worst = weighted_utilization_georg_worst.set_index("test_case")

    weighted_utilization_georg_avg = filtered_georg_data[["test_case", "weighted_utilization_overall"]].groupby("test_case").mean().rename(
        {"weighted_utilization_overall": "avg_georg"}, axis=1)

    weighted_utilization_plot_data = pd.concat(
        [weighted_utilization_erich, weighted_utilization_georg_best, weighted_utilization_georg_avg, weighted_utilization_georg_worst], axis=1)
    weighted_utilization_plot_axis = weighted_utilization_plot_data.plot.bar()
    weighted_utilization_plot_axis.set_xlabel("Test Case")
    weighted_utilization_plot_axis.set_ylabel("Weighted Utilization Rate")
    plt.title("Total Weighted Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    weighted_utilization_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_overall.png"))
    weighted_utilization_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_overall.svg"))

    # Weighted utilization reserved
    weighted_utilization_reserved_erich = filtered_erich_data[["test_case", "weighted_utilization_reserved"]].rename({"weighted_utilization_reserved": "erich"},
                                                                                                                     axis=1)
    weighted_utilization_reserved_erich = weighted_utilization_reserved_erich.set_index("test_case")

    weighted_utilization_reserved_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "weighted_utilization_reserved"]].rename(
        {"weighted_utilization_reserved": "best_georg"}, axis=1)
    weighted_utilization_reserved_georg_best = weighted_utilization_reserved_georg_best.set_index("test_case")

    weighted_utilization_reserved_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][
        ["test_case", "weighted_utilization_reserved"]].rename({"weighted_utilization_reserved": "worst_georg"}, axis=1)
    weighted_utilization_reserved_georg_worst = weighted_utilization_reserved_georg_worst.set_index("test_case")

    weighted_utilization_reserved_georg_avg = filtered_georg_data[["test_case", "weighted_utilization_reserved"]].groupby("test_case").mean().rename(
        {"weighted_utilization_reserved": "avg_georg"}, axis=1)

    weighted_utilization_reserved_plot_data = pd.concat(
        [weighted_utilization_reserved_erich, weighted_utilization_reserved_georg_best, weighted_utilization_reserved_georg_avg,
         weighted_utilization_reserved_georg_worst], axis=1)
    weighted_utilization_reserved_plot_axis = weighted_utilization_reserved_plot_data.plot.bar()
    weighted_utilization_reserved_plot_axis.set_xlabel("Test Case")
    weighted_utilization_reserved_plot_axis.set_ylabel("Weighted Utilization Rate")
    plt.title("Weighted Reserved Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    weighted_utilization_reserved_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_reserved.png"))
    weighted_utilization_reserved_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_reserved.svg"))

    # On-demand
    weighted_utilization_on_demand_erich = filtered_erich_data[["test_case", "weighted_utilization_on_demand"]].rename(
        {"weighted_utilization_on_demand": "erich"}, axis=1)
    weighted_utilization_on_demand_erich = weighted_utilization_on_demand_erich.set_index("test_case")

    weighted_utilization_on_demand_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "weighted_utilization_on_demand"]].rename(
        {"weighted_utilization_on_demand": "best_georg"}, axis=1)
    weighted_utilization_on_demand_georg_best = weighted_utilization_on_demand_georg_best.set_index("test_case")

    weighted_utilization_on_demand_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][
        ["test_case", "weighted_utilization_on_demand"]].rename({"weighted_utilization_on_demand": "worst_georg"}, axis=1)
    weighted_utilization_on_demand_georg_worst = weighted_utilization_on_demand_georg_worst.set_index("test_case")

    weighted_utilization_on_demand_georg_avg = filtered_georg_data[["test_case", "weighted_utilization_on_demand"]].groupby("test_case").mean().rename(
        {"weighted_utilization_on_demand": "avg_georg"}, axis=1)

    weighted_utilization_on_demand_plot_data = pd.concat(
        [weighted_utilization_on_demand_erich, weighted_utilization_on_demand_georg_best, weighted_utilization_on_demand_georg_avg,
         weighted_utilization_on_demand_georg_worst], axis=1)
    weighted_utilization_on_demand_plot_axis = weighted_utilization_on_demand_plot_data.plot.bar()
    weighted_utilization_on_demand_plot_axis.set_xlabel("Test Case")
    weighted_utilization_on_demand_plot_axis.set_ylabel("Weighted Utilization Rate")
    plt.title("Weighted On-Demand Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    weighted_utilization_on_demand_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_on_demand.png"))
    weighted_utilization_on_demand_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_on_demand.svg"))

    # Spot
    weighted_utilization_spot_erich = filtered_erich_data[["test_case", "weighted_utilization_spot"]].rename({"weighted_utilization_spot": "erich"}, axis=1)
    weighted_utilization_spot_erich = weighted_utilization_spot_erich.set_index("test_case")

    weighted_utilization_spot_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "weighted_utilization_spot"]].rename(
        {"weighted_utilization_spot": "best_georg"}, axis=1)
    weighted_utilization_spot_georg_best = weighted_utilization_spot_georg_best.set_index("test_case")

    weighted_utilization_spot_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "weighted_utilization_spot"]].rename(
        {"weighted_utilization_spot": "worst_georg"}, axis=1)
    weighted_utilization_spot_georg_worst = weighted_utilization_spot_georg_worst.set_index("test_case")

    weighted_utilization_spot_georg_avg = filtered_georg_data[["test_case", "weighted_utilization_spot"]].groupby("test_case").mean().rename(
        {"weighted_utilization_spot": "avg_georg"}, axis=1)

    weighted_utilization_spot_plot_data = pd.concat(
        [weighted_utilization_spot_erich, weighted_utilization_spot_georg_best, weighted_utilization_spot_georg_avg, weighted_utilization_spot_georg_worst],
        axis=1)
    weighted_utilization_spot_plot_axis = weighted_utilization_spot_plot_data.plot.bar()
    weighted_utilization_spot_plot_axis.set_xlabel("Test Case")
    weighted_utilization_spot_plot_axis.set_ylabel("Weighted Utilization Rate")
    plt.title("Weighted Spot Utilization Rate")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    weighted_utilization_spot_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_spot.png"))
    weighted_utilization_spot_plot_axis.figure.savefig(output_path.joinpath("weighted_utilization_spot.svg"))

    # Total cost
    cost_overall_erich = filtered_erich_data[["test_case", "cost_overall"]].rename({"cost_overall": "erich"}, axis=1)
    cost_overall_erich = cost_overall_erich.set_index("test_case")

    cost_overall_georg_best = filtered_georg_data[filtered_georg_data["rank"] == 0][["test_case", "cost_overall"]].rename({"cost_overall": "best_georg"},
                                                                                                                          axis=1)
    cost_overall_georg_best = cost_overall_georg_best.set_index("test_case")

    cost_overall_georg_worst = filtered_georg_data[filtered_georg_data["rank"] == worst_rank][["test_case", "cost_overall"]].rename(
        {"cost_overall": "worst_georg"}, axis=1)
    cost_overall_georg_worst = cost_overall_georg_worst.set_index("test_case")

    cost_overall_georg_avg = filtered_georg_data[["test_case", "cost_overall"]].groupby("test_case").mean().rename({"cost_overall": "avg_georg"}, axis=1)

    cost_overall_plot_data = pd.concat([cost_overall_erich, cost_overall_georg_best, cost_overall_georg_avg, cost_overall_georg_worst], axis=1)
    cost_overall_plot_axis = cost_overall_plot_data.plot.bar(log=True)
    cost_overall_plot_axis.set_xlabel("Test Case")
    cost_overall_plot_axis.set_ylabel("Cost")
    plt.title("Total Cost")
    plt.suptitle('')
    plt.subplots_adjust(bottom=0.2)
    cost_overall_plot_axis.figure.savefig(output_path.joinpath("cost_overall_log.png"))
    cost_overall_plot_axis.figure.savefig(output_path.joinpath("cost_overall_log.svg"))

    # Generational data
    gen_df = pd.read_csv(f"./results/generations_{label}.csv", delimiter=",")
    # Get best individuals for all generations
    gen_plot_data = gen_df[(gen_df["rank"] == 0) & (gen_df["iteration"] == 2) & (gen_df["test_case"] == "case_6")]
    # Get worst individuals for all generations
    gen_plot_data = gen_plot_data.assign(worst_cost=gen_df[(gen_df["test_case"] == "case_6")
                                                           & (gen_df["iteration"] == 2)].groupby("generation")["cost_overall"].max().values)
    # Calculate average performance for all generations
    gen_groups = gen_df[(gen_df["test_case"] == "case_6") & (gen_df["iteration"] == 2)].groupby("generation")
    gen_plot_data = gen_plot_data.assign(avg_cost=gen_groups["cost_overall"].mean().values)
    gen_plot_data = gen_plot_data.rename(columns={"cost_overall": "best_cost"})
    gen_plot_data = gen_plot_data[["generation", "best_cost", "worst_cost", "avg_cost"]]
    gen_plot_axis = gen_plot_data.plot.line(x="generation")
    gen_plot_axis.set_xlabel("Generation")
    gen_plot_axis.set_ylabel("Cost")
    plt.title("Cost per Generation")
    plt.suptitle('')
    gen_plot_axis.figure.savefig(output_path.joinpath("cost_generation.png"))
    gen_plot_axis.figure.savefig(output_path.joinpath("cost_generation.svg"))

    # Cost information
    erich_instances = pd.read_csv(f"./results/erich_instances_{label}.csv",
                                  parse_dates=["beginning_time", "ending_time", "availability_start", "availability_end"],
                                  delimiter=",")
    georg_instances = pd.read_csv(f"./results/georg_instances_{label}.csv",
                                  parse_dates=["beginning_time", "ending_time", "availability_start", "availability_end"],
                                  delimiter=",")

    filtered_erich_instances = erich_instances[erich_instances["iteration"] == rand_iteration]
    filtered_georg_instances = georg_instances[georg_instances["iteration"] == rand_iteration]

    filtered_erich_instances = filtered_erich_instances.assign(price_capcity=None)
    erich_res = filtered_erich_instances[filtered_erich_instances["market_space"] == "reserved"]
    filtered_erich_instances.loc[filtered_erich_instances["market_space"] == "reserved", "price_capacity"] = (erich_res["price"] / erich_res["capacity"]) / ((erich_res["availability_end"] - erich_res["availability_start"]) / pd.Timedelta("1H"))
    erich_non_res = filtered_erich_instances[filtered_erich_instances["market_space"] != "reserved"]
    filtered_erich_instances.loc[filtered_erich_instances["market_space"] != "reserved", "price_capacity"] = (erich_non_res["price"] / erich_non_res["capacity"])

    filtered_georg_instances = filtered_georg_instances.assign(price_capcity=None)
    georg_res = filtered_georg_instances[filtered_georg_instances["market_space"] == "reserved"]
    filtered_georg_instances.loc[filtered_georg_instances["market_space"] == "reserved", "price_capacity"] = (georg_res["price"] / georg_res["capacity"]) / ((georg_res["availability_end"] - georg_res["availability_start"]) / pd.Timedelta("1H"))
    georg_non_res = filtered_georg_instances[filtered_georg_instances["market_space"] != "reserved"]
    filtered_georg_instances.loc[filtered_georg_instances["market_space"] != "reserved", "price_capacity"] = (georg_non_res["price"] / georg_non_res["capacity"])

    price_capacity_erich = filtered_erich_instances[["test_case", "price_capacity"]].groupby("test_case").mean().rename({"price_capacity": "erich"}, axis=1)
    # price_capacity_erich = price_capacity_erich.set_index("test_case")

    price_capacity_georg_best = filtered_georg_instances[filtered_georg_instances["rank"] == 0][["test_case", "price_capacity"]].groupby("test_case").mean().rename({"price_capacity": "best_georg"}, axis=1)
    # price_capacity_georg_best = price_capacity_georg_best.set_index("test_case")

    price_capacity_georg_worst = filtered_georg_instances[filtered_georg_instances["rank"] == worst_rank][["test_case", "price_capacity"]].groupby("test_case").mean().rename({"price_capacity": "worst_georg"}, axis=1)
    # price_capacity_georg_worst = price_capacity_georg_worst.set_index("test_case")

    price_capacity_georg_avg = filtered_georg_instances[["test_case", "price_capacity"]].groupby("test_case").mean().rename({"price_capacity": "avg_georg"}, axis=1)
    # price_capacity_georg_avg = price_capacity_georg_avg.set_index("test_case")

    price_capacity_plot_data = pd.concat([price_capacity_erich, price_capacity_georg_best, price_capacity_georg_avg, price_capacity_georg_worst], axis=1)
    price_capacity_plot_axis = price_capacity_plot_data.plot.bar()
    price_capacity_plot_axis.set_xlabel("Test Case")
    price_capacity_plot_axis.set_ylabel("Avg. Cost per Capacity Unit")
    plt.title("Average Cost per Capacity Unit")
    plt.suptitle("")
    plt.subplots_adjust(bottom=0.2)
    price_capacity_plot_axis.figure.savefig(output_path.joinpath("average_price_capacity.png"))
    price_capacity_plot_axis.figure.savefig(output_path.joinpath("average_price_capacity.svg"))

    # plt.show()

    # for group in grouped_data:
    #     data
    # data.plot.bar()
    # plt.show()

    # with open("./results/erich_test_run.json", "r") as f:
    #     erich_data = json.load(f)
    #
    # with open("./results/georg_test_run.json", "r") as f:
    #     georg_data = json.load(f)
    #
    # # Plot runtime based on: https://stackoverflow.com/questions/16592222/matplotlib-group-boxplots
    # ticks = [key for key in erich_data.keys()]
    # run_time_data_erich = [test_case["run_time"] for test_case in erich_data.values()]
    # run_time_data_georg = [test_case["run_time"] for test_case in georg_data.values()]
    #
    # plt.figure()
    # bpl = plt.boxplot(run_time_data_erich, positions=np.array(range(len(run_time_data_erich))) * 2.0 - 0.4, sym='', widths=0.6)
    # bpr = plt.boxplot(run_time_data_georg, positions=np.array(range(len(run_time_data_georg))) * 2.0 + 0.4, sym='', widths=0.6)
    # set_box_color(bpl, '#D7191C')
    # set_box_color(bpr, '#2C7BB6')
    #
    # plt.plot([], c='#D7191C', label='Erich')
    # plt.plot([], c='#2C7BB6', label='Georg')
    # plt.legend()
    #
    # plt.xticks(range(0, len(ticks) * 2, 2), ticks)
    # plt.xlim(-2, len(ticks) * 2)
    # plt.ylim(bottom=2)
    # plt.tight_layout()
    # plt.savefig("./figures/runtime_comparison.png")
    # plt.savefig("./figures/runtime_comparison.svg", format="svg")
