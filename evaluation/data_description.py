import pandas as pd

app_config = {
    "apps_1": "data/applications_20_week.csv",
    "apps_2": "data/applications_100_week.csv",
    "apps_3": "data/applications_20_month.csv",
    "apps_4": "data/applications_100_month.csv",
    "apps_5": "data/applications_20_year.csv",
    "apps_6": "data/applications_100_year.csv",
}

app_test_sets = {}
for key, data_path in app_config.items():
    app_test_sets[key] = pd.read_csv(data_path, parse_dates=["starting_time", "finishing_time"])

app_df = pd.DataFrame(index=app_test_sets.keys())

for key, data in app_test_sets.items():
    app_df.loc[key, "total_number_of_applications"] = len(data)
    app_df.loc[key, "non_preemptible"] = len(data[data["preemptible"] == False])
    app_df.loc[key, "preemptible"] = len(data[data["preemptible"] == True])
    app_df.loc[key, "avg_res_demand"] = data["res_demand"].mean()
    app_df.loc[key, "std_res_demand"] = data["res_demand"].std()
    app_df.loc[key, "avg_res_demand_deviation"] = data["res_demand_deviation"].mean()
    app_df.loc[key, "std_res_demand_deviation"] = data["res_demand_deviation"].std()
    allocation_periods = ((data["finishing_time"] - data["starting_time"]) / pd.Timedelta(value=1, unit="H"))
    app_df.loc[key, "avg_allocation_periods"] = allocation_periods.mean()
    app_df.loc[key, "std_allocation_periods"] = allocation_periods.std()

app_df = app_df.round({"non_preemptible": 2,
                       "preemptible": 2,
                       "avg_res_demand": 2,
                       "std_res_demand": 2,
                       "avg_res_demand_deviation": 2,
                       "std_res_demand_deviation": 2,
                       "avg_allocation_periods": 2,
                       "std_allocation_periods": 2})

app_df.to_csv("./results/application_summary.csv")
app_df.to_latex("./results/application_summary.tex")

type_config = {
    "types_1": "data/instance_types_week.csv",
    "types_2": "data/instance_types_month.csv",
    "types_3": "data/instance_types_year.csv"
}

types_test_sets = {}
for key, data_path in type_config.items():
    types_test_sets[key] = pd.read_csv(data_path, parse_dates=["availability_start", "availability_end"])

types_df = pd.DataFrame(index=types_test_sets.keys())

for key, data in types_test_sets.items():
    types_df.loc[key, "total_number_of_instance_types"] = len(data)
    types_df.loc[key, "avg_capacity"] = data["capacity"].mean()
    types_df.loc[key, "std_capacity"] = data["capacity"].std()
    reserved = data[data["market_space"].str.match("reserved")]
    availability_periods = (reserved["availability_end"] - reserved["availability_start"]) / pd.Timedelta(value=1, unit="H")
    types_df.loc[key, "avg_reserved_price"] = (reserved["price"] / availability_periods).mean()
    types_df.loc[key, "std_reserved_price"] = (reserved["price"] / availability_periods).std()
    types_df.loc[key, "avg_on_demand_price"] = data[data["market_space"].str.match("on_demand")]["price"].mean()
    types_df.loc[key, "std_on_demand_price"] = data[data["market_space"].str.match("on_demand")]["price"].std()
    types_df.loc[key, "avg_spot_price"] = data[data["market_space"].str.match("spot")]["price"].mean()
    types_df.loc[key, "std_spot_price"] = data[data["market_space"].str.match("spot")]["price"].std()

types_df = types_df.round({col: 2 for col in types_df.columns})

types_df.to_csv("./results/instance_types_summary.csv")
types_df.to_latex("./results/instance_types_summary.tex")
