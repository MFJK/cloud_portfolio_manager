import random
import uuid

import numpy as np
import pandas as pd

from pathlib import Path

from tests.helper import generate_random_dates
from cloud_portfolio_manager import InstanceType, Application


def generate_instance_types(price_per_capacity,
                            price_range,
                            number_of_instances,
                            reserved_start_lower_bound,
                            reserved_start_upper_bound,
                            reserved_end_lower_bound,
                            reserved_end_upper_bound,
                            time_unit,
                            output):
    instance_types = []
    for _ in range(number_of_instances):

        # Create a tiny bit of variation in the prices
        tmp_price_per_capacity = price_per_capacity * random.uniform(1 - price_range, 1 + price_range)

        data = {}
        data["instance_type_id"] = str(uuid.uuid4())
        data["capacity"] = np.random.randint(1, np.random.choice([16, 32, 64, 128], p=[0.9, 0.07, 0.02, 0.01]))
        data["market_space"] = random.choice(["reserved", "on_demand", "spot"])

        if data["market_space"] == "on_demand":
            data["price"] = round(data["capacity"] * tmp_price_per_capacity, 4)
        elif data["market_space"] == "spot":
            # Spot up to 90% savings
            data["price"] = round(
                data["capacity"] * (tmp_price_per_capacity * random.uniform(np.random.choice([0.70, 0.60, 0.4, 0.1], p=[0.60, 0.18, 0.12, 0.1]), 0.9)), 4)
        elif data["market_space"] == "reserved":
            # Reserved up to 72% savings
            data["price"] = round(
                data["capacity"] * (tmp_price_per_capacity * random.uniform(np.random.choice([0.6, 0.5, 0.45, 0.28], p=[0.60, 0.18, 0.12, 0.1]), 0.72)), 4)

        if data["market_space"] == "reserved":
            data["availability_start"] = random.choice(generate_random_dates(reserved_start_lower_bound, reserved_start_upper_bound))
            data["availability_end"] = random.choice(generate_random_dates(reserved_end_lower_bound, reserved_end_upper_bound))

            data["availability_start"] = data["availability_start"].ceil(freq=f"{time_unit[0]}{time_unit[1]}")
            data["availability_end"] = data["availability_end"].floor(freq=f"{time_unit[0]}{time_unit[1]}")

            # Reserved instances have a total price
            data["price"] = round(data["price"] * ((data["availability_end"] - data["availability_start"]) / pd.Timedelta(*time_unit)), 4)

        instance_types.append(InstanceType(**data))

    df = pd.DataFrame(data=[instance_type.to_dict() for instance_type in instance_types])
    df.to_csv(output, index=False)


def generate_applications(start_lower_bound,
                          end_upper_bound,
                          number_of_applications,
                          output,
                          freq="1H"):
    applications = []
    for _ in range(number_of_applications):
        data = {}
        data["application_id"] = str(uuid.uuid4())
        data["preemptible"] = np.random.choice([True, False], p=[0.5, 0.5])
        data["res_demand"] = round(random.uniform(0, np.random.choice([5, 13, 16, 20], p=[0.88, 0.08, 0.03, 0.01])), 2)
        data["res_demand_deviation"] = round(random.uniform(0, np.random.choice(
            [data["res_demand"] / 4, data["res_demand"] / 3, data["res_demand"] / 2, data["res_demand"]], p=[0.5, 0.3, 0.15, 0.05])), 2)
        dates = generate_random_dates(start_lower_bound, end_upper_bound, 2)
        data["starting_time"] = min(dates).floor(freq=freq)
        data["finishing_time"] = max(dates).floor(freq=freq)

        applications.append(Application(**data))

    df = pd.DataFrame(data=[application.to_dict() for application in applications])
    df.to_csv(output, index=False)


if __name__ == "__main__":
    output_path = Path(__file__).parent.joinpath("data")

    # Instance types for a week
    generate_instance_types(price_per_capacity=0.35,
                            price_range=0.5,
                            number_of_instances=500,
                            reserved_start_lower_bound=pd.Timestamp("2020-12-31T00:00:00Z"),
                            reserved_start_upper_bound=pd.Timestamp("2021-01-02T00:00:00Z"),
                            reserved_end_lower_bound=pd.Timestamp("2021-01-06T00:00:00Z"),
                            reserved_end_upper_bound=pd.Timestamp("2021-01-09T00:00:00Z"),
                            time_unit=(1, "H"),
                            output=output_path.joinpath("instance_types_week.csv"))

    # Instance types for a month
    generate_instance_types(price_per_capacity=0.35,
                            price_range=0.5,
                            number_of_instances=500,
                            reserved_start_lower_bound=pd.Timestamp("2020-12-31T00:00:00Z"),
                            reserved_start_upper_bound=pd.Timestamp("2021-01-02T00:00:00Z"),
                            reserved_end_lower_bound=pd.Timestamp("2021-01-30T00:00:00Z"),
                            reserved_end_upper_bound=pd.Timestamp("2021-02-01T00:00:00Z"),
                            time_unit=(1, "H"),
                            output=output_path.joinpath("instance_types_month.csv"))

    # Instance types for a year
    generate_instance_types(price_per_capacity=0.35,
                            price_range=0.5,
                            number_of_instances=500,
                            reserved_start_lower_bound=pd.Timestamp("2020-12-31T00:00:00Z"),
                            reserved_start_upper_bound=pd.Timestamp("2021-01-02T00:00:00Z"),
                            reserved_end_lower_bound=pd.Timestamp("2021-12-30T00:00:00Z"),
                            reserved_end_upper_bound=pd.Timestamp("2022-01-01T00:00:00Z"),
                            time_unit=(1, "H"),
                            output=output_path.joinpath("instance_types_year.csv"))

    # Generate 20 applications for a week
    # generate_applications(start_lower_bound=pd.Timestamp("2021-01-01T00:00:00Z"),
    #                       end_upper_bound=pd.Timestamp("2021-01-08T00:00:00Z"),
    #                       number_of_applications=20,
    #                       output=output_path.joinpath("applications_20_week.csv"))

    # Generate 100 applications for a week
    # generate_applications(start_lower_bound=pd.Timestamp("2021-01-01T00:00:00Z"),
    #                       end_upper_bound=pd.Timestamp("2021-01-08T00:00:00Z"),
    #                       number_of_applications=100,
    #                       output=output_path.joinpath("applications_100_week.csv"))

    # Generate 20 applications for a week
    # generate_applications(start_lower_bound=pd.Timestamp("2021-01-01T00:00:00Z"),
    #                       end_upper_bound=pd.Timestamp("2021-01-31T00:00:00Z"),
    #                       number_of_applications=20,
    #                       output=output_path.joinpath("applications_20_month.csv"))

    # Generate 100 applications for a week
    # generate_applications(start_lower_bound=pd.Timestamp("2021-01-01T00:00:00Z"),
    #                       end_upper_bound=pd.Timestamp("2021-01-31T00:00:00Z"),
    #                       number_of_applications=100,
    #                       output=output_path.joinpath("applications_100_month.csv"))

    # Generate 20 applications for a week
    # generate_applications(start_lower_bound=pd.Timestamp("2021-01-01T00:00:00Z"),
    #                       end_upper_bound=pd.Timestamp("2021-12-31T00:00:00Z"),
    #                       number_of_applications=20,
    #                       output=output_path.joinpath("applications_20_year.csv"))

    # Generate 100 applications for a week
    # generate_applications(start_lower_bound=pd.Timestamp("2021-01-01T00:00:00Z"),
    #                       end_upper_bound=pd.Timestamp("2021-12-31T00:00:00Z"),
    #                       number_of_applications=100,
    #                       output=output_path.joinpath("applications_100_year.csv"))
